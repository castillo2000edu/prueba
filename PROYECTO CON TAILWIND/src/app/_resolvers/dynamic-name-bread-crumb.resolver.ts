import { Injectable } from '@angular/core'
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router'
import { Observable, of } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class DynamicNameBreadCrumbResolver implements Resolve<any> {
  breadcrumb: Object

  constructor(private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    this.updateState()
    const _name: string | null = this.breadcrumb['breadcrumb']
    // const _name: string | null = route.paramMap.get('display')
    return of(_name)
  }

  updateState() {
    let state = this.router.getCurrentNavigation().extras.state || history.state
    if ('breadcrumb' in state) {
      sessionStorage.setItem('breadcrumb', JSON.stringify(state))
    }
    this.breadcrumb = JSON.parse(sessionStorage.getItem('breadcrumb'))
  }
}

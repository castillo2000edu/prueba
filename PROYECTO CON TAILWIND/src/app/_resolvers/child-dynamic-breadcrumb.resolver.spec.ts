import { TestBed } from '@angular/core/testing'

import { ChildDynamicBreadcrumbResolver } from './child-dynamic-breadcrumb.resolver'

describe('ChildDynamicBreadcrumbResolver', () => {
  let resolver: ChildDynamicBreadcrumbResolver

  beforeEach(() => {
    TestBed.configureTestingModule({})
    resolver = TestBed.inject(ChildDynamicBreadcrumbResolver)
  })

  it('should be created', () => {
    expect(resolver).toBeTruthy()
  })
})

import { TestBed } from '@angular/core/testing'

import { DynamicNameBreadCrumbResolver } from './dynamic-name-bread-crumb.resolver'

describe('DynamicNameBreadCrumbResolver', () => {
  let resolver: DynamicNameBreadCrumbResolver

  beforeEach(() => {
    TestBed.configureTestingModule({})
    resolver = TestBed.inject(DynamicNameBreadCrumbResolver)
  })

  it('should be created', () => {
    expect(resolver).toBeTruthy()
  })
})

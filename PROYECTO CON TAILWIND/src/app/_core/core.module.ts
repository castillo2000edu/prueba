import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { LayoutComponent } from './components/layout/layout.component'
import { HeaderComponent } from './components/header/header.component'
import { MaterialModule } from '../material/material.module'
import { RouterModule } from '@angular/router'
import { SearchFormComponent } from './components/search-form/search-form.component'
import { ReactiveFormsModule } from '@angular/forms'
import { ResourcesTabComponent } from './components/resources-tab/resources-tab.component'
import { MatTabsModule } from '@angular/material/tabs'
import { TextFieldComponent } from './components/text-field/text-field.component'
import { PageHeadingComponent } from './components/page-heading/page-heading.component'
import { FooterComponent } from './components/footer/footer.component'
import { SelectFieldComponent } from './components/select-field/select-field.component'
import { BreadcrumbModule } from 'angular-crumbs'

@NgModule({
  declarations: [
    LayoutComponent,
    HeaderComponent,
    SearchFormComponent,
    ResourcesTabComponent,
    TextFieldComponent,
    PageHeadingComponent,
    FooterComponent,
    SelectFieldComponent
  ],
  exports: [
    LayoutComponent,
    SearchFormComponent,
    TextFieldComponent,
    PageHeadingComponent,
    FooterComponent,
    HeaderComponent,
    ResourcesTabComponent,
    SelectFieldComponent
  ],
  imports: [CommonModule, RouterModule, MaterialModule, ReactiveFormsModule, MatTabsModule, BreadcrumbModule]
})
export class CoreModule {}

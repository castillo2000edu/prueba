export interface MenuItem {
  id: string
  title: string
  icon: string
  link: string
  hint: string
}

export const MenuNavigation: MenuItem[] = [
  {
    id: 'admin.institutions',
    title: 'Instituciones y jefes de proyecto',
    icon: 'home',
    link: '/institution/general/institutions',
    hint: 'Lorem ipsum dolor sit inst, consectetur adipiscing elit. Aenean euismod bibendum laoreet.'
  },
  {
    id: 'admin.services',
    title: 'Servicios',
    icon: 'cloud',
    link: '/institution/general/services',
    hint: 'Lorem ipsum dolor sit serv, consectetur adipiscing elit. Aenean euismod bibendum laoreet.'
  },
  {
    id: 'application.list',
    title: 'Aplicaciones',
    icon: 'settings',
    link: '/application/list',
    hint: 'Lorem ipsum dolor sit application, consectetur adipiscing elit. Aenean euismod bibendum laoreet.'
  },
  // {
  //     id: 'institution.datos_generales',
  //     title: 'Datos generales',
  //     icon: 'text_snippet',
  //     link: '/institution/general'
  // },
  {
    id: 'institution.apps',
    title: 'Aplicaciones',
    icon: 'settings',
    link: '/institution/general/apps',
    hint: 'Lorem ipsum dolor sit app, consectetur adipiscing elit. Aenean euismod bibendum laoreet.'
  },
  {
    id: 'institution.desarrolladores',
    title: 'Desarrolladores',
    icon: 'people',
    link: '/institution/general/developer',
    hint: 'Lorem ipsum dolor sit dev, consectetur adipiscing elit. Aenean euismod bibendum laoreet.'
  }
]

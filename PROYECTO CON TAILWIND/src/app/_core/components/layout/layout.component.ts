import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { BreadcrumbService } from 'angular-crumbs'
import { Subject } from 'rxjs'
import { MenuItem } from './menu.data'
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  private _unsubscribeAll: Subject<any>

  isLoading: boolean

  dataNavigation: MenuItem[]

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private breadcrumbService: BreadcrumbService
  ) {
    this._unsubscribeAll = new Subject()
  }

  ngOnInit(): void {
    // // this.breadcrumbService.changeBreadcrumb(this.activatedRoute.snapshot, 'pruebaa');
    // this.activatedRoute.queryParams.subscribe(params => {
    //   if ('name' in params) {
    //     console.log(params['name'])
    //     // this.activatedRoute.data['value']['breadcrumb'] = params['name']
    //     this.breadcrumbService.changeBreadcrumb(this.activatedRoute.snapshot, params['name']);
    //   }
    //  }
    // )
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }
}

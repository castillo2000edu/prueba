import { Component, OnInit } from '@angular/core'
import { ThemePalette } from '@angular/material/core'
import { MatTabChangeEvent } from '@angular/material/tabs'
import { Router } from '@angular/router'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { SecurityService } from 'src/app/_services/security.service'
import { MenuItem, MenuNavigation } from '../layout/menu.data'

@Component({
  selector: 'app-resources-tab',
  templateUrl: './resources-tab.component.html',
  styleUrls: ['./resources-tab.component.scss']
})
export class ResourcesTabComponent implements OnInit {
  activeLink: string = '/institution/general/institutions'
  background: ThemePalette = undefined

  private unsubscribeAll: Subject<any>
  dataNavigation: MenuItem[]
  showTabs: boolean = true

  constructor(private securityService: SecurityService, private router: Router) {
    this.unsubscribeAll = new Subject()
  }

  ngOnInit(): void {
    this.loadMenuData()
    this.activeLink = this.router.url
    this.router.events.subscribe(val => {
      if ('url' in val) {
        this.activeLink = val.url
      }
    })
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next()
    this.unsubscribeAll.complete()
  }

  toggleBackground() {
    this.background = this.background ? undefined : 'primary'
  }

  loadMenuData() {
    this.securityService.currentUser$.pipe(takeUntil(this.unsubscribeAll)).subscribe(user => {
      this.setDataNavigation(user)
    })
  }

  setDataNavigation(user: any) {
    const adminData = MenuNavigation.filter(item => item.id.includes('admin'))
    const instData = MenuNavigation.filter(item => item.id.includes('institution') && !item.id.includes('admin'))
    const appData = MenuNavigation.filter(item => item.id.includes('application'))
    this.dataNavigation = []
    if (user.perfil.includes('ADMIN')) {
      this.dataNavigation.push(...adminData)
    }
    if (user.perfil.includes('INSTITUTION-USER')) {
      this.dataNavigation.push(...instData)
    }
    if (user.perfil.includes('APP-USER') && !user.perfil.includes('INSTITUTION-USER')) {
      this.dataNavigation.push(...appData)
    }
  }

  onTabChanged(event: MatTabChangeEvent) {}
}

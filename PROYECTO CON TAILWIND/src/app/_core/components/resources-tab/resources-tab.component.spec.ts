import { ComponentFixture, TestBed } from '@angular/core/testing'

import { ResourcesTabComponent } from './resources-tab.component'

describe('ResourcesTabComponent', () => {
  let component: ResourcesTabComponent
  let fixture: ComponentFixture<ResourcesTabComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ResourcesTabComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcesTabComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

import { Component, Input, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { SecurityService } from '../../../_services/security.service'
import { Observable, Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { UsuarioModel } from '../../../_models/usuario.model'
import { BreadcrumbService } from 'xng-breadcrumb'
import { JwtHelperService } from '@auth0/angular-jwt'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title = 'ID Gob.pe para desarrolladores'
  user: UsuarioModel | null
  fullName: string
  role: string
  sigla: string

  breadcrumb$: Observable<any[]>

  private _unsubscribeAll: Subject<any>

  constructor(
    private bcService: BreadcrumbService,
    private _securityService: SecurityService,
    private _router: Router
  ) {
    this._unsubscribeAll = new Subject()
  }

  ngOnInit(): void {
    this.breadcrumb$ = this.bcService.breadcrumbs$
    this.getUser()
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  logout() {
    this._securityService.signOut()
    this._router.navigate(['/inicio'])
  }

  private getUser() {
    this._securityService.currentUser$.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.fullName = this.capitalize(x.first_name)
      this.sigla = this.fullName?.charAt(0)
      if (x.perfil.includes('ADMIN')) {
        this.role = 'Administrador'
      } else if (x.perfil.includes('INSTITUTION-USER')) {
        this.role = 'Institución'
      } else {
        this.role = 'Aplicación'
      }
    })
  }

  // ToDo: create functions availables for all controllers
  capitalize(word: string) {
    const lower = word.toLowerCase()
    return word.charAt(0).toUpperCase() + lower.slice(1)
  }

  hasLogin() {
    const helper = new JwtHelperService()
    return this._securityService.accessToken && !helper.isTokenExpired(this._securityService.accessToken)
  }
}

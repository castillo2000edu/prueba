import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  @Input() isLoading!: boolean
  @Input() placeholder: string
  @Input() label: string
  @Output() onSubmit = new EventEmitter<string>()

  formGroup: FormGroup | null

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.createFormGroup()
  }

  handleSubmit() {
    if (!this.isLoading) {
      const word = this.formGroup.get('filter').value
      this.onSubmit.emit(this.getFilterValue(word))
    }
  }

  private getFilterValue(filterValue: string) {
    filterValue = filterValue?.trim()
    filterValue = filterValue?.toLowerCase()
    return filterValue
  }

  private createFormGroup() {
    this.formGroup = this._formBuilder.group({
      filter: ['']
    })
  }
}

import {
  AfterViewInit,
  Component,
  Directive,
  ElementRef,
  forwardRef,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'
import { EventEmitter } from '@angular/core'

@Directive({
  selector: '[someDir]'
})
export class SomeDir {}

@Component({
  selector: 'app-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['./text-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextFieldComponent),
      multi: true
    }
  ]
})
export class TextFieldComponent implements OnInit, AfterViewInit, ControlValueAccessor {
  @Input() id: string
  @Input() label: string
  @Input() placeHolder: string = ''
  @Input() isValid: boolean = true
  @Input() errorText: string

  @Input() contentIsDisabled: boolean
  @Input() hasIconSearch: boolean
  @Input() btnIsDisabled: boolean
  @Input() isLoading: boolean
  // ToDo: mejorar paso de props con ViewChild
  @Input() maxLength: string = ''

  @Output() inputKeyDownEnter = new EventEmitter()

  value: string
  isDisabled: boolean
  onChange = (_: any) => {}
  onTouch = () => {}

  onInput(value: string) {
    this.value = value
    this.onTouch()
    this.onChange(this.value)
  }

  @ViewChild(SomeDir) inpElementRef!: ElementRef

  // errors: Array<any> = []

  constructor(private elementRef: ElementRef) {}
  ngAfterViewInit() {}
  ngOnInit() {
    const attributes = this.elementRef.nativeElement.attributes
  }

  writeValue(value: any): void {
    if (value) {
      this.value = value || ''
    } else {
      this.value = ''
    }
  }
  registerOnChange(fn: any): void {
    this.onChange = fn
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled
  }
}

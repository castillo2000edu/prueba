import { Component, OnInit, Input, forwardRef } from '@angular/core'
import { FormBuilder, FormGroup, Validators, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

@Component({
  selector: 'app-select-field',
  templateUrl: './select-field.component.html',
  styleUrls: ['./select-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectFieldComponent),
      multi: true
    }
  ]
})
export class SelectFieldComponent implements OnInit, ControlValueAccessor {
  @Input() id: string
  @Input() label: string
  @Input() isValid: boolean = true
  @Input() errorText: string
  @Input() entities: any[]

  @Input() group: FormGroup
  @Input() controlName: string

  value: string
  isDisabled: boolean
  onChange = (_: any) => {}
  onTouch = () => {}

  onInput(value: string) {
    this.value = value
    this.onTouch()
    this.onChange(this.value)
  }

  constructor() {}

  ngOnInit(): void {}

  writeValue(value: any): void {
    if (value) {
      this.value = value || ''
    } else {
      this.value = ''
    }
  }
  registerOnChange(fn: any): void {
    this.onChange = fn
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled
  }
}

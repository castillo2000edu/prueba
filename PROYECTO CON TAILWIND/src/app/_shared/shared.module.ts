import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { MaterialModule } from '../material/material.module'
import { ReactiveFormsModule } from '@angular/forms'
import { PopupConfirmComponent } from './components/popup-confirm/popup-confirm.component'
import { PopupTextValidateComponent } from './components/popup-text-validate/popup-text-validate.component'
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component'
import { CardComponent } from './components/card/card.component'
import { BodyDirective } from '../_helpers/_directivas/body.directive'
import { BodyOutletDirective } from '../_helpers/_directivas/body-outlet.directive'
import { ButtonComponent } from './components/button/button.component'
import { IconButtonComponent } from './components/icon-button/icon-button.component'
import { ToggleComponent } from './components/toggle/toggle.component'
import { CoreModule } from '../_core/core.module'
import { MessageComponent } from './components/message/message.component'

const components = [
  PopupConfirmComponent,
  PopupTextValidateComponent,
  LoadingSpinnerComponent,
  CardComponent,
  BodyDirective,
  BodyOutletDirective,
  ButtonComponent,
  IconButtonComponent,
  ToggleComponent,
  MessageComponent
]

@NgModule({
  declarations: components,
  exports: components,
  imports: [CommonModule, MaterialModule, ReactiveFormsModule, CoreModule]
})
export class SharedModule {}

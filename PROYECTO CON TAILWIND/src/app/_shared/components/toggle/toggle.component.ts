import { Component, OnInit, Input, forwardRef, Output } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'
import { EventEmitter } from '@angular/core'
import { MatSlideToggleChange } from '@angular/material/slide-toggle'

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ToggleComponent),
      multi: true
    }
  ]
})
export class ToggleComponent implements OnInit, ControlValueAccessor {
  @Input() id: string
  @Input() checked: Boolean
  @Input() hasEventEmitter: boolean

  @Output() changeToggle = new EventEmitter()

  value: boolean
  isDisabled: boolean

  onChange = (_: any) => {}
  onTouch = () => {}

  onInput(event: MatSlideToggleChange) {
    this.value = event.source.checked
    this.onTouch()
    this.onChange(this.value)
  }

  constructor() {}
  writeValue(value: any): void {
    if (value) {
      this.value = value || false
    } else {
      this.value = false
    }
  }
  registerOnChange(fn: any): void {
    this.onChange = fn
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled
  }

  ngOnInit(): void {}
}

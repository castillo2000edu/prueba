import { Component, Input, OnInit, Output } from '@angular/core'
import { EventEmitter } from '@angular/core'

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  @Input() message: string
  @Input() messageType: string
  @Output() messageClose = new EventEmitter()

  constructor() {}

  ngOnInit(): void {}
}

import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Subject } from 'rxjs'
import { map, takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-base-message',
  templateUrl: './base-message.component.html',
  styleUrls: ['./base-message.component.scss']
})
export class BaseMessageComponent implements OnInit {
  showMessage: boolean
  textMessage: string
  typeMessage: string

  stateParams: any

  private _unsubscribeParams: Subject<any>

  constructor(private activatedRoutes: ActivatedRoute) {
    this._unsubscribeParams = new Subject()
  }

  ngOnInit(): void {
    this.getStateParams()
    this.callMessage()
  }

  getStateParams() {
    this.activatedRoutes.params
      .pipe(map(() => window.history.state))
      .pipe(takeUntil(this._unsubscribeParams))
      .subscribe(x => {
        this.stateParams = x
      })
  }

  callMessage() {
    if (this.stateParams.message) {
      this.showMessage = true
      this.typeMessage = this.stateParams.typeMessage
      this.textMessage = this.stateParams.message
    }
  }

  setMessage(typeMessage: string, textMessage: string, showMessage: boolean) {
    this.showMessage = showMessage
    this.typeMessage = typeMessage
    this.textMessage = textMessage
    this.scrollToElement('.showMessageClass')
  }

  scrollToElement(classElement: string) {
    document.querySelector(classElement).scrollIntoView({ behavior: 'smooth', block: 'start' })
  }

  closeMessage() {
    this.showMessage = false
  }
}

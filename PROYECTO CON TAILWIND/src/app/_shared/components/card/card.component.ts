import { AfterContentInit, Component, ContentChild, Input, OnInit, ViewChild } from '@angular/core'
import { BodyOutletDirective } from 'src/app/_helpers/_directivas/body-outlet.directive'
import { BodyDirective } from 'src/app/_helpers/_directivas/body.directive'

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements AfterContentInit {
  @Input() title: string
  @Input() subtitle: string
  @ViewChild(BodyOutletDirective, { static: true }) bodyOutlet: BodyOutletDirective
  @ContentChild(BodyDirective) body: BodyDirective

  constructor() {}

  ngAfterContentInit(): void {
    this.bodyOutlet.viewContainer.createEmbeddedView(this.body.template)
  }
}

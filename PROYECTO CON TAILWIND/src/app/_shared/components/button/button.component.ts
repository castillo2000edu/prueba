import { Component, Input, OnInit } from '@angular/core'

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() type: String = 'submit'
  @Input() typeButton: String
  @Input() text: String
  @Input() disabled: Boolean
  @Input() className: String
  @Input() hasIconLoader: Boolean
  @Input() hasIcon: Boolean
  @Input() icon: String
  constructor() {}

  ngOnInit(): void {}
}

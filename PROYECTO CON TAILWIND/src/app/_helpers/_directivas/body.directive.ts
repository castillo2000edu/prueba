import { Directive, TemplateRef } from '@angular/core'

@Directive({
  selector: '[appBody]'
})
export class BodyDirective {
  constructor(public template: TemplateRef<any>) {}
}

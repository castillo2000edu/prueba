import { Directive, ViewContainerRef } from '@angular/core'

@Directive({
  selector: '[appBodyOutlet]'
})
export class BodyOutletDirective {
  constructor(public viewContainer: ViewContainerRef) {}
}

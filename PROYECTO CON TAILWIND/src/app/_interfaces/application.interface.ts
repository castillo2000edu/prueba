export interface IApplicationItem {
  _id: string
  organization_code: string
  client_id: string
  name: string
  description: string
  url: string
  logo_url: string
  editable: boolean
  enabled: boolean
  created_at: Date
}

export interface IAppOrganization {
  code?: string
  name?: string
  acronym?: string
  domains?: string
}

export interface IAppUpdateRequest {
  id: string
  name: string
  description: string
  url: string
}

export interface IAppItemResponse {
  client_id: string
  created_at: string
  editable: string
  enabled: string
  logo_url: string
  name: string
  description: string
  _id: string
}

export interface IAppOneResponse {
  _id: string
  organization_code?: string
  client_id?: string
  name?: string
  description?: string
  url?: string
  logo_url?: string
  editable?: boolean
  enabled?: boolean
  created_at?: Date
}

export interface IAppInfoOrgResponse {
  _id: string
  code: string
  name: string
  acronym: string
  domains: string[]
  created_at: Date
}

export interface IAppInfoDevResponse {
  doc: string
  names: string
  lastname: string
  created_at: Date | string
}

export interface IAppInfoResponse {
  app: IAppOneResponse
  organization: IAppInfoOrgResponse
  developer: IAppInfoDevResponse
}

export interface IServiceOneResponse {
  _id: string
  client_id: string
  client_secret: string
  organization_code: string
  service_code: string
  tos_url: string
  policy_url: string
  redirect_uris: any[]
  js_origin_uris: any[]
  mac_authorized?: any[]
  editable: boolean
  created_at: Date
}

export interface IServiceUpdateRequest {
  id: string
  service_code?: string
  mac_authorized?: string
  tos_url?: string
  policy_url?: string
  redirect_uris?: string
  js_origin_uris?: string
}

export interface IServiceEnabledRequest {
  id: string
  service_code: string
}

export interface IServiceUpdateRequest {
  id: string
  service_code?: string
  mac_authorized?: string
  tos_url?: string
  policy_url?: string
  redirect_uris?: string
  js_origin_uris?: string
}

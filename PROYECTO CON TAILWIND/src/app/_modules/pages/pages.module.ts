import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { PagesRoutingModule } from './pages-routing.module'
import { InicioComponent } from './components/inicio/inicio.component'
import { MaterialModule } from 'src/app/material/material.module'
import { SharedModule } from 'src/app/_shared/shared.module'
import { CoreModule } from 'src/app/_core/core.module'

@NgModule({
  declarations: [InicioComponent],
  imports: [CommonModule, PagesRoutingModule, MaterialModule, SharedModule, CoreModule]
})
export class PagesModule {}

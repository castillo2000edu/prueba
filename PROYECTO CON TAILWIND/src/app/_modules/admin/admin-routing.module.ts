import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { EditarinstitucionComponent } from './components/editarinstitucion/editarinstitucion.component'
import { EditarjefeproyectosComponent } from './components/editarjefeproyectos/editarjefeproyectos.component'
import { InstitucionesComponent } from './components/instituciones/instituciones.component'
import { JefeproyectosComponent } from './components/jefeproyectos/jefeproyectos.component'
import { NuevainstitucionComponent } from './components/nuevainstitucion/nuevainstitucion.component'
import { NuevojefeproyectosComponent } from './components/nuevojefeproyectos/nuevojefeproyectos.component'
import { ServiciosComponent } from './components/servicios/servicios.component'
import { VersionesComponent } from './components/versiones/versiones.component'

const lorem = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`

const routes: Routes = []

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}

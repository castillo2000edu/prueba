import { Component, Input, OnInit, ViewChild } from '@angular/core'

import { MatDialog } from '@angular/material/dialog'
import { MatTable } from '@angular/material/table'
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { Subject } from 'rxjs'
import { JwtHelperService } from '@auth0/angular-jwt'
import { IInstitutionData, IOrganizationUpdateRequest } from 'src/app/_interfaces/institution.interface'
import { ActivatedRoute, Router } from '@angular/router'
import { BusyService } from 'src/app/_services/busy.service'
import { takeUntil } from 'rxjs/operators'
import { OrganizationService } from 'src/app/_services/organization.service'
import { RegexPatterns } from 'src/app/_shared/regexPatterns'
import { MessageService } from 'src/app/_shared/services/message.service'
import { ICatalogAdmin } from 'src/app/_interfaces/catalog.interface'
import { IOrganization } from 'src/app/_interfaces/institution.interface'
import { TranslateService } from '@ngx-translate/core'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { BaseMessageComponent } from 'src/app/_shared/components/base-message/base-message.component'

@Component({
  selector: 'app-nuevainstitucion',
  templateUrl: './nuevainstitucion.component.html',
  styleUrls: ['./nuevainstitucion.component.scss']
})
export class NuevainstitucionComponent extends BaseMessageComponent implements OnInit {
  showPage = false
  isLoading = false
  isServiceLoading = false
  isDisabledButton: boolean = true
  formGroup: FormGroup | null
  private _unsubscribeAll: Subject<any>
  @ViewChild(MatTable) _matTable: MatTable<any>
  currentService: IOrganization | null

  @Input() isEditForm: boolean = false
  private institutionId: string | null
  private institutionData: IInstitutionData | null

  constructor(
    private _messageService: MessageService,
    private _busyService: BusyService,
    public dialog: MatDialog,
    private _formBuilder: FormBuilder,
    private _organizationService: OrganizationService,
    private router: Router,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute
  ) {
    super(activatedRoute)
    this._unsubscribeAll = new Subject()
    this.translate.setDefaultLang('es')
    this.translate.use('es')
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  ngOnInit() {
    this.institutionId = this.activatedRoute.snapshot.paramMap.get('id') ?? null
    this.listenLoadingEvent()
    this.createForm()
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  private async getInstituion() {
    if (!this.institutionId) {
      this.cancel()
    } else {
      const response = await this._organizationService.getAdminOrganizationInfo(this.institutionId).toPromise()
      if (response.success) {
        this.institutionData = response.data
        this.setInstitutionForm()
      }
    }
  }

  private setInstitutionForm() {
    const item = this.institutionData
    if (item?.organization) {
      const _org = item.organization
      this.formGroup.get('code').setValue(_org.code)
      this.formGroup.get('name').setValue(_org.name)
      this.formGroup.get('acronym').setValue(_org.acronym)
      this.domainFormArray.clear()
      _org.domains.forEach(domain => {
        this.domainFormArray.push(this.newDomainFormGroup(domain))
      })
    }
    if (item?.services) {
      const controls = this.getServiceArrayForm
      controls.controls.forEach((ctrl: FormGroup) => {
        const _serviceValue = item.services.filter(x => x.service_code === ctrl.controls['code'].value)
        if (_serviceValue) {
          ctrl.controls['enabled'].setValue(_serviceValue[0].enabled)
          ctrl.controls['blocked'].setValue(_serviceValue[0].blocked)
        }
      })
    }
    this.showPage = true
  }

  async orgSearch() {
    if (this.isLoading) {
      return false
    }
    if (this.isInValidField('code')) {
      return false
    }

    const _code = this.formGroup.controls['code'].value

    if (_code == null || _code?.length < 11) {
      this.formGroup.get('token').setValue(null)
      this.setMessage(ERROR_MESSAGE, 'Debe de ingresar un RUC válido.', true)
      return false
    }

    if (_code) {
      const response = await this._organizationService.getAdminOrganizationSearch(_code).toPromise()
      if (response?.success) {
        const helper = new JwtHelperService()
        const rucItem: { ruc; name } = helper.decodeToken(response.token)
        this.formGroup.get('token').setValue(response.token)
        this.formGroup.get('code').setValue(rucItem.ruc)
        this.formGroup.get('name').setValue(rucItem.name)
        this.formGroup.controls['acronym'].enable()
        this.formGroup.controls['domains'].enable()
        this.isDisabledButton = false
      } else if (response?.error) {
        this.setMessage(ERROR_MESSAGE, response?.error.message, true)
      } else {
        this.setMessage(ERROR_MESSAGE, 'Debe de ingresar un RUC válido.', true)
      }
    }
  }

  save() {
    if (this.isEditForm) {
      return this.update()
    }
    this.insert()
  }

  private async update() {
    if (!this.formGroup.valid) {
      return false
    }
    const formValue = this.formGroup.value
    const request: IOrganizationUpdateRequest = {
      id: this.institutionId,
      acronym: formValue.acronym,
      domains: formValue.domains
        .filter(item => item?.domain !== null && item?.domain?.trim().length > 0)
        .map((item: any) => item.domain)
        .join(','),
      services: formValue.services.map(({ code, enabled, blocked }) => ({ code, enabled, blocked }))
    }
    const response = await this._organizationService.putAdminOrganizationUpdate(request).toPromise()
    if (response.success) {
      this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', false)
      this.cancel()
    } else if (response?.error) {
      this.setMessage(ERROR_MESSAGE, response?.error.message, true)
    }
  }

  cancel() {
    const message = this.textMessage
    const typeMessage = this.typeMessage
    this.router.navigate(['/institution/general/institutions'], { state: { message, typeMessage } })
  }

  private async insert() {
    if (!this.formGroup.valid) {
      return false
    }
    const formValue = this.formGroup.value
    const domains = (formValue.domains as [])
      .filter((x: { domain }) => x.domain !== null)
      .map((x: { domain }) => x.domain.trim())
      .join(',')
    const request: IOrganizationUpdateRequest = {
      token: formValue.token,
      acronym: formValue.acronym,
      domains: domains,
      services: formValue.services.map(({ code, enabled, blocked }) => ({ code, enabled, blocked }))
    }
    const _response = await this._organizationService.postAdminOrganizationInsert(request).toPromise()
    if (_response.success) {
      this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', false)
      this.cancel()
    } else if (_response?.error) {
      this.setMessage(ERROR_MESSAGE, _response?.error.message, true)
    }
  }

  private createForm() {
    this.formGroup = this._formBuilder.group({
      code: this.isEditForm ? [null] : [null, [Validators.required, Validators.pattern(RegexPatterns.RUC)]],
      token: this.isEditForm ? [null] : [null, [Validators.required]],
      name: this.isEditForm ? [null] : [null, [Validators.required]],
      acronym: [null, [Validators.required, Validators.maxLength(100)]],
      domains: new FormArray([this.newDomainFormGroup(null)]),
      services: new FormArray([])
    })

    this.formGroup.controls['name'].disable()
    if (!this.isEditForm) {
      this.formGroup.controls['acronym'].disable()
      this.formGroup.controls['domains'].disable()
    } else {
      this.isDisabledButton = false
      this.formGroup.controls['code'].disable()
    }
    this.getServicesList()
  }

  get domainFormArray(): FormArray {
    return this.formGroup.get('domains') as FormArray
  }

  addDomain() {
    const length = (this.domainFormArray.value as []).length
    if (length < 3) {
      this.domainFormArray.push(this.newDomainFormGroup(null))
    }
  }

  private newDomainFormGroup(domain: string) {
    return this._formBuilder.group({
      domain: [domain, [Validators.pattern(RegexPatterns.DOMAIN)]]
    })
  }

  isDomainErrorByCtrl(control: FormControl) {
    return control.invalid
  }

  isInValidField(strField: string): boolean {
    const field = this.formGroup.get(strField)
    return field.invalid && field.touched
  }

  async getServicesList() {
    this.isServiceLoading = true
    const response = await this._organizationService.getAdminCatalogAdmin('services')
    const services = response.filter(item => item.category === 'services')
    const _services = services?.sort((a: ICatalogAdmin, b: ICatalogAdmin) => (a.order > b.order ? 1 : -1))
    const controls = this.getServiceArrayForm

    _services.forEach((service, index) => {
      controls.push(
        this._formBuilder.group({
          num: [index + 1],
          code: [service.code],
          value: [service.value],
          enabled: [false],
          blocked: [false]
        })
      )
    })

    this.isServiceLoading = false
    if (this.isEditForm) {
      this.getInstituion()
    } else {
      this.showPage = true
    }
  }

  get getServiceArrayForm() {
    return this.formGroup.get('services') as FormArray
  }

  setDomainId(num: number) {
    return `DOMAIN${num}`
  }

  setUrlLabel(num: number) {
    return num >= 1 ? `Dominio - ${num + 1}` : `Dominio principal`
  }

  showError(control: FormControl) {
    return control.invalid && control.touched
  }

  getErrorMessage(control: FormControl, strField: string) {
    const field = control.get('domain')
    let messageError = ''
    if (field.errors) {
      if (field.errors.hasOwnProperty('required') && field.errors.required) {
        messageError = `Ingresar ${this.getTranslate(strField)} `
      } else {
        messageError =
          strField == 'filter'
            ? `Ingresar y buscar ${this.getTranslate(strField)}`
            : `Ingresar ${this.getTranslate(strField)} válido`
      }
    }
    return messageError
  }

  getTranslate(word: string): string {
    let translateWord
    this.translate.get(word).subscribe((res: string) => {
      translateWord = res
    })
    return translateWord
  }

  removeDomain(arrayName: string, i: number) {
    const array = this.formGroup.controls[arrayName] as FormArray
    array.removeAt(i)
    this.formGroup.markAsDirty()
  }

  setTitle() {
    return `${this.isEditForm ? 'Editar' : 'Nueva'} institución`
  }

  setSubtitle() {
    return `Completa los campos para ${this.isEditForm ? 'editar la institución' : 'crear una nueva institución'}`
  }
}

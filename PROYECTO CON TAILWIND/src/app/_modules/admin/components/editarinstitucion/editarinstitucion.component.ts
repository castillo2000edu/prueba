import { Component, OnInit, ViewChild } from '@angular/core'

import { MatDialog } from '@angular/material/dialog'
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms'

import { ActivatedRoute, Router } from '@angular/router'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { OrganizationService } from 'src/app/_services/organization.service'
import { MatTable } from '@angular/material/table'
import { BusyService } from 'src/app/_services/busy.service'
import { IInstitutionData, IOrganizationUpdateRequest } from 'src/app/_interfaces/institution.interface'
import { RegexPatterns } from 'src/app/_shared/regexPatterns'
import { MessageService } from 'src/app/_shared/services/message.service'
import { ICatalogAdmin } from 'src/app/_interfaces/catalog.interface'
import { NuevainstitucionComponent } from '../nuevainstitucion/nuevainstitucion.component'

@Component({
  selector: 'app-editarinstitucion',
  templateUrl: './editarinstitucion.component.html',
  styleUrls: ['./editarinstitucion.component.scss']
})
export class EditarinstitucionComponent extends NuevainstitucionComponent implements OnInit {
  ngOnInit() {
    super.ngOnInit()
  }
}

import { Component, OnInit } from '@angular/core'
import { NuevojefeproyectosComponent } from '../nuevojefeproyectos/nuevojefeproyectos.component'

@Component({
  selector: 'app-editarjefeproyectos',
  templateUrl: './editarjefeproyectos.component.html',
  styleUrls: ['./editarjefeproyectos.component.scss']
})
export class EditarjefeproyectosComponent extends NuevojefeproyectosComponent implements OnInit {
  ngOnInit() {
    super.ngOnInit()
  }
}

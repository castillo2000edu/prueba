import { Component, OnInit } from '@angular/core'

import { Subject } from 'rxjs'
import { map, takeUntil } from 'rxjs/operators'
import { ActivatedRoute, Router } from '@angular/router'
import { IOrganization } from 'src/app/_interfaces/institution.interface'
import { IPagination } from 'src/app/_interfaces/pagination.interface'
import { OrganizationService } from 'src/app/_services/organization.service'
import { BusyService } from 'src/app/_services/busy.service'
import { BaseMessageComponent } from 'src/app/_shared/components/base-message/base-message.component'

@Component({
  selector: 'app-instituciones',
  templateUrl: './instituciones.component.html',
  styleUrls: ['./instituciones.component.scss']
})
export class InstitucionesComponent extends BaseMessageComponent implements OnInit {
  displayedColumns = ['num', 'ruc', 'nombre', 'fecha', 'opciones']
  dataSource: IPagination<IOrganization>
  page = 1
  count = 10
  word = ''

  showPage = false
  isLoading: boolean

  private _unsubscribeAll: Subject<any>

  constructor(
    private _organizationService: OrganizationService,
    private _busyService: BusyService,
    private _router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    super(activatedRoute)
    this._unsubscribeAll = new Subject()
  }

  ngOnInit() {
    this.listenLoadingEvent()

    this.getDataSource()
    super.ngOnInit()
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  buscar(word) {
    if (!this.isLoading) {
      this.page = 1
      this.word = word
      this.getDataSource()
    }
  }

  async getDataSource() {
    const data = await this._organizationService.getAdminOrganization(this.word, this.page, this.count).toPromise()
    this.dataSource = data
    this.showPage = true
  }

  get pageIndex() {
    return this.page < 0 ? 0 : this.page - 1
  }

  pageEvent(page: any) {
    this.page = page
    this.getDataSource()
  }

  getRowNumber(index: number) {
    return this.page == 1 ? index + 1 : 1 + index + (this.page - 1) * this.count
  }

  edit(institution: IOrganization) {
    const _id = institution._id
    this._router.navigate(['/institution/general/institutions/edit', _id])
  }

  editProjectManager(institution: IOrganization) {
    const _orgCode = institution.code
    const name = institution.name
    const breadcrumb = `Jefes de proyecto de ${institution.acronym}`
    this._router.navigate(['/institution/general/institutions/project-manager', _orgCode], {
      state: { name, breadcrumb }
    })
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }
}

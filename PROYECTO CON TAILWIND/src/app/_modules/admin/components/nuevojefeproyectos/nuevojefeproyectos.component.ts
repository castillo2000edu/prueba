import { Component, Directive, Input, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { takeUntil } from 'rxjs/operators'
import { Subject } from 'rxjs'
import { JwtHelperService } from '@auth0/angular-jwt'
import { RegexPatterns } from 'src/app/_shared/regexPatterns'
import { BusyService } from 'src/app/_services/busy.service'
import { IManagerInsertRequest, IManagerItem, IManagerUpdateRequest } from 'src/app/_interfaces/manager.interface'
import { ManagerService } from 'src/app/_services/manager.service'
import { MessageService } from '../../../../_shared/services/message.service'
import { TranslateService } from '@ngx-translate/core'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { BaseMessageComponent } from 'src/app/_shared/components/base-message/base-message.component'

@Component({
  selector: 'app-nuevojefeproyectos',
  templateUrl: '../../../institution/components/new-developer/new-developer.component.html',
  styleUrls: ['./nuevojefeproyectos.component.scss']
})
export class NuevojefeproyectosComponent extends BaseMessageComponent implements OnInit {
  formGroup: FormGroup | null

  private organizationCode: string | null
  private token: string | null
  private _unsubscribeAll: Subject<any>

  isLoading = false
  isSaving = false
  showPage = false

  @Input() isEditForm: boolean = false
  private projectManagerId: string | null
  private projectManager: IManagerItem | null

  constructor(
    private _messageService: MessageService,
    private _busyService: BusyService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _formBuilder: FormBuilder,
    private _managerService: ManagerService,
    private translate?: TranslateService
  ) {
    super(activatedRoute)
    this._unsubscribeAll = new Subject()
    this.translate.setDefaultLang('es')
    this.translate.use('es')
  }

  ngOnInit(): void {
    this.listenLoadingEvent()
    this.organizationCode = this.activatedRoute.snapshot.paramMap.get('code') ?? null
    this.projectManagerId = this.activatedRoute.snapshot.paramMap.get('id') ?? null
    if (!this.organizationCode) {
      this.cancel()
    }
    this.createForm()
    if (this.isEditForm) {
      this.getProjectManager()
    }
  }

  private async getProjectManager() {
    if (!(this.organizationCode && this.projectManagerId)) {
      this.router.navigate(['/institution/general/institutions'])
      return false
    }
    const response = await this._managerService.getAdminManagerOne(this.projectManagerId).toPromise()
    if (response.success) {
      this.projectManager = response.Item
      const _lastName = `${this.projectManager.lastname_1} ${this.projectManager.lastname_2} ${this.projectManager.lastname_3}`
      this.formGroup.get('filter').setValue(this.projectManager.doc)
      this.formGroup.get('name').setValue(this.projectManager.names)
      this.formGroup.get('lastName').setValue(_lastName)
      this.formGroup.get('phone').setValue(this.projectManager.phone)
      this.formGroup.get('email').setValue(this.projectManager.email)
      this.formGroup.get('position').setValue(this.projectManager.position)
    }
    this.showPage = true
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }
  isInValidField(strField: string): boolean {
    const field = this.formGroup.get(strField)
    return field.invalid && field.touched
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  private createForm() {
    this.formGroup = this._formBuilder.group({
      filter: [null, [Validators.required, Validators.pattern(RegexPatterns.DNI)]],
      token: this.isEditForm ? [null] : [null, [Validators.required]],
      name: [null],
      lastName: [null],
      phone: [null, [Validators.required, Validators.pattern(RegexPatterns.CELULAR)]],
      email: [null, [Validators.required, Validators.pattern(RegexPatterns.EMAIL)]],
      position: [null, [Validators.required, Validators.maxLength(200)]]
    })

    this.formGroup.controls['name'].disable()
    this.formGroup.controls['lastName'].disable()
    if (!this.isEditForm) {
      this.showPage = true
    }
  }

  async buscar() {
    if (this.isLoading) {
      return false
    }
    if (this.isInValidField('filter')) {
      return false
    }
    const filter = this.formGroup.controls['filter'].value
    const _filter: string = filter?.trim()

    if (_filter == null || _filter?.length < 8) {
      this.formGroup.get('token').setValue(null)
      this.setMessage(ERROR_MESSAGE, 'Debe de ingresar un DNI válido.', true)
      return false
    }
    const response = await this._managerService.getAdminManagerSearch(filter).toPromise()
    if (response?.success) {
      this.token = response.token
      const helper = new JwtHelperService()
      const personItem: { name; lastname_1; lastname_2; lastname_3 } = helper.decodeToken(response.token)
      const _lastNameArray = [personItem.lastname_1, personItem.lastname_2, personItem.lastname_3]
      const lastName = _lastNameArray.join(' ')

      const extraData = response.data ?? null
      const phone = extraData?.phone ?? null
      const email = extraData?.email ?? null
      const position = extraData?.position ?? null

      this.formGroup.get('phone').setValue(phone)
      this.formGroup.get('email').setValue(email)
      this.formGroup.get('position').setValue(position)

      this.formGroup.get('name').setValue(personItem.name)
      this.formGroup.get('lastName').setValue(lastName?.trim())
      this.formGroup.get('token').setValue(this.token)
    } else if (response?.error) {
      this.setMessage(ERROR_MESSAGE, response?.error.message, true)
    } else {
      this.setMessage(ERROR_MESSAGE, 'Debe de ingresar un DNI válido.', true)
    }
  }

  cancel() {
    const message = this.textMessage
    const typeMessage = this.typeMessage
    const _orgCode = this.organizationCode
    this.router.navigate(['/institution/general/institutions/project-manager', _orgCode], {
      state: { message, typeMessage }
    })
  }

  setTitle() {
    return `${this.isEditForm ? 'Editar' : 'Nuevo'} jefe de proyecto`
  }

  setSubtitle() {
    return `Completa los campos para ${
      this.isEditForm ? 'editar el jefe de proyecto' : 'crear un nuevo jefe de proyecto'
    }`
  }

  getError(strField: string): string {
    const field = this.formGroup.get(strField)
    let messageError = ''
    if (field.errors) {
      if (field.errors.hasOwnProperty('required') && field.errors.required) {
        messageError = `Ingresar ${this.getTranslate(strField)} `
      } else {
        messageError =
          strField == 'filter'
            ? `Ingresar y buscar ${this.getTranslate(strField)}`
            : `Ingresar ${this.getTranslate(strField)} válido`
      }
    }
    return messageError
  }

  getTranslate(word: string): string {
    let translateWord
    this.translate.get(word).subscribe((res: string) => {
      translateWord = res
    })
    return translateWord
  }

  async save() {
    if (this.isEditForm) {
      return this.update()
    }
    if (this.formGroup.valid) {
      const _formData: { phone; email; position } = this.formGroup.value
      const _request: IManagerInsertRequest = {
        organization_code: this.organizationCode,
        token: this.token,
        phone: _formData.phone,
        email: _formData.email,
        position: _formData.position
      }
      this.isSaving = true

      const _response = await this._managerService.postAdminManagerInsert(_request).toPromise()

      if (_response.success) {
        this.isSaving = false
        this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', false)
        this.cancel()
      } else if (_response?.error) {
        this.setMessage(ERROR_MESSAGE, _response?.error.message, true)
      }
    }
  }

  private async update() {
    if (this.projectManager) {
      const request: IManagerUpdateRequest = {
        id: this.projectManagerId,
        email: this.formGroup.value.email,
        phone: this.formGroup.value.phone,
        position: this.formGroup.value.position
      }
      this.isSaving = true
      const _response = await this._managerService.putAdminManagerUpdate(request).toPromise()
      if (_response.success) {
        this.isSaving = false
        this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', false)
        this.cancel()
      } else if (_response?.error) {
        this.setMessage(ERROR_MESSAGE, _response?.error.message, true)
      }
    }
  }
}

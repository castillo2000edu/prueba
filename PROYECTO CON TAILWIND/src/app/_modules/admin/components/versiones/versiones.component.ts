import { Component, OnInit } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { ActivatedRoute, Router } from '@angular/router'
import { Subject } from 'rxjs'
import { map, takeUntil } from 'rxjs/operators'
import { IPagination } from 'src/app/_interfaces/pagination.interface'
import { MatSlideToggleChange } from '@angular/material/slide-toggle'
import { BusyService } from 'src/app/_services/busy.service'
import { IVersionRequest, IVersionResponse } from 'src/app/_interfaces/version.interface'
import { VersionService } from 'src/app/_services/version.service'
import { VersionComponent } from '../version/version.component'
import { MessageService } from '../../../../_shared/services/message.service'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { BaseMessageComponent } from 'src/app/_shared/components/base-message/base-message.component'

@Component({
  selector: 'app-versiones',
  templateUrl: './versiones.component.html',
  styleUrls: ['./versiones.component.scss']
})
export class VersionesComponent extends BaseMessageComponent implements OnInit {
  code: string
  page = 1
  count = 10
  showPage: boolean
  isLoading: boolean
  isTableLoading: boolean
  dataSource: IPagination<IVersionResponse>
  private service_code: string | null
  private _unsubscribeAll: Subject<any>

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _messageService: MessageService,
    private _versionService: VersionService,
    private _busyService: BusyService
  ) {
    super(activatedRoute)
    this._unsubscribeAll = new Subject()
    this.getServiceId()
  }

  ngOnInit() {
    this.code = this.activatedRoute.snapshot.paramMap.get('code')
    this.listenLoadingEvent()
    this.getVersionsList()
    super.ngOnInit()
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  private getServiceId() {
    this.service_code = this.activatedRoute.snapshot.paramMap.get('code') ?? null
  }

  private async getVersionsList() {
    if (!this.service_code) {
      this.router.navigate(['/institution/general/institutions/services'])
    } else {
      this.getDataSource()
    }
  }

  private async getDataSource() {
    this.isTableLoading = true
    const response = await this._versionService
      .getAdminVersion(this.service_code, null, this.page, this.count)
      .toPromise()
    this.isTableLoading = false

    if (response.success) {
      this.dataSource = response
    }

    this.showPage = true
  }

  get pageIndex() {
    return this.page < 0 ? 0 : this.page - 1
  }

  pageEvent(page: any) {
    this.page = page
    this.getDataSource()
  }

  getRowNumber(index: number) {
    return this.page == 1 ? index + 1 : 1 + index + (this.page - 1) * this.count
  }

  new() {
    const dialogRef = this.dialog.open(VersionComponent, {
      data: {
        metodo: 'Nueva'
      },
      width: '98%',
      maxWidth: '450px'
    })

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(result => {
        if (result && result.version_number) {
          this.insertVersion(result.version_number)
        }
      })
  }

  edit(item: IVersionResponse) {
    const _id = item._id
    this.router.navigate(['/institution/general/services', this.code, 'versions', 'edit', _id])
  }

  async toggleChange(event: MatSlideToggleChange, row: IVersionResponse) {
    event.source.checked = await this.changeStatus(event.checked, row._id)
  }

  private async changeStatus(value: boolean, id: string): Promise<boolean> {
    const param: IVersionRequest = { value, id }
    const response = await this._versionService.putAdminVersionChangeStatus(param).toPromise()
    if (response?.success) {
      this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito.', true)
      return value
    } else if (response?.error) {
      this.setMessage(ERROR_MESSAGE, response?.error.message, true)
    }
    return !value
  }

  private async insertVersion(versionNumber: string) {
    const request: IVersionRequest = { service_code: this.service_code, version_number: versionNumber }
    const response = await this._versionService.postAdminVersionInsert(request).toPromise()

    if (response.success) {
      this._messageService.showSuccessRegister()
      this.getDataSource()
    } else if (response?.error) {
      this.setMessage(ERROR_MESSAGE, response?.error.message, true)
    }
  }

  private async updateVersion(id: string, version_number: string) {
    const request: IVersionRequest = { id, version_number }
    const response = await this._versionService.putAdminVersionUpdate(request).toPromise()

    if (response.success) {
      this._messageService.showSuccessRegister()

      this.getDataSource()
    } else if (response?.error) {
      this._messageService.showWarning(response?.error.message)
    }
  }
}

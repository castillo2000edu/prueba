import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ChildDynamicBreadcrumbResolver } from 'src/app/_resolvers/child-dynamic-breadcrumb.resolver'
import { DynamicNameBreadCrumbResolver } from 'src/app/_resolvers/dynamic-name-bread-crumb.resolver'
import { EditarinstitucionComponent } from '../admin/components/editarinstitucion/editarinstitucion.component'
import { EditarjefeproyectosComponent } from '../admin/components/editarjefeproyectos/editarjefeproyectos.component'
import { InstitucionesComponent } from '../admin/components/instituciones/instituciones.component'
import { JefeproyectosComponent } from '../admin/components/jefeproyectos/jefeproyectos.component'
import { NuevainstitucionComponent } from '../admin/components/nuevainstitucion/nuevainstitucion.component'
import { NuevojefeproyectosComponent } from '../admin/components/nuevojefeproyectos/nuevojefeproyectos.component'
import { ServiciosComponent } from '../admin/components/servicios/servicios.component'
import { VersionesComponent } from '../admin/components/versiones/versiones.component'
import { EditVersionComponent } from '../application/components/edit-version/edit-version.component'
import { NewVersionComponent } from '../application/components/new-version/new-version.component'
import { AppsComponent } from './components/apps/apps.component'
import { DatosgeneralesComponent } from './components/datosgenerales/datosgenerales.component'
import { DeveloperComponent } from './components/developer/developer.component'
import { EditDeveloperComponent } from './components/edit-developer/edit-developer.component'
import { EditarAppComponent } from './components/editar-app/editar-app.component'
import { NewDeveloperComponent } from './components/new-developer/new-developer.component'

const lorem = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`

const routes: Routes = [
  {
    path: 'general',
    data: { breadcrumb: 'Inicio', roles: ['INSTITUTION-USER', 'ADMIN'] },
    children: [
      { path: '', data: { roles: ['INSTITUTION-USER', 'ADMIN'] }, component: DatosgeneralesComponent },
      {
        path: 'institutions',
        data: { breadcrumb: 'Instituciones', roles: ['ADMIN'] },
        children: [
          { path: '', component: InstitucionesComponent },
          {
            path: 'new',
            data: { breadcrumb: 'Nueva institución' },
            component: NuevainstitucionComponent
          },
          {
            path: 'edit/:id',
            data: { breadcrumb: 'Editar institución' },
            component: EditarinstitucionComponent
          },
          {
            path: 'project-manager/:code',
            resolve: { breadcrumb: DynamicNameBreadCrumbResolver },
            children: [
              { path: '', component: JefeproyectosComponent },
              {
                path: 'new',
                data: { breadcrumb: 'Nuevo jefe de proyecto' },
                resolve: { breadcrumb: ChildDynamicBreadcrumbResolver },
                component: NuevojefeproyectosComponent
              },
              {
                path: 'edit/:id',
                data: { breadcrumb: 'Editar jefe de proyecto' },
                resolve: { breadcrumb: ChildDynamicBreadcrumbResolver },
                component: EditarjefeproyectosComponent
              }
            ]
          },
          { path: '**', component: InstitucionesComponent }
        ]
      },
      {
        path: 'services',
        data: { breadcrumb: 'Servicios', roles: ['ADMIN'] },
        children: [
          { path: '', component: ServiciosComponent },
          {
            path: ':code/versions',
            data: { breadcrumb: 'Versiones' },
            children: [
              { path: '', data: { breadcrumb: 'Versiones' }, component: VersionesComponent },
              { path: 'new', data: { breadcrumb: 'Nueva versión' }, component: NewVersionComponent },
              {
                path: 'edit/:id',
                data: { breadcrumb: 'Editar versión' },
                component: EditVersionComponent
              }
            ]
          },
          { path: '**', component: ServiciosComponent }
        ]
      },
      {
        path: 'apps',
        data: { breadcrumb: 'Aplicaciones', roles: ['INSTITUTION-USER'] },
        children: [
          { path: '', component: AppsComponent },
          {
            path: 'edit/:id',
            data: { breadcrumb: 'Editar aplicación', roles: ['INSTITUTION-USER'] },
            component: EditarAppComponent
          }
        ]
      },

      {
        path: 'developer',
        data: { breadcrumb: 'Desarrolladores', roles: ['INSTITUTION-USER'] },
        children: [
          { path: '', component: DeveloperComponent },
          {
            path: 'new',
            data: { breadcrumb: 'Nuevo desarrollador', roles: ['INSTITUTION-USER'] },
            component: NewDeveloperComponent
          },
          {
            path: 'edit/:id',
            data: { breadcrumb: 'Editar desarrollador', roles: ['INSTITUTION-USER'] },
            component: EditDeveloperComponent
          }
        ]
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstitutionRoutingModule {}

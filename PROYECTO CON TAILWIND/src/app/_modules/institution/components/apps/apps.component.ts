import { Component, OnInit } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { Subject } from 'rxjs'
import { BusyService } from 'src/app/_services/busy.service'
import { ActivatedRoute, Router } from '@angular/router'
import { IPagination } from 'src/app/_interfaces/pagination.interface'
import { map, takeUntil } from 'rxjs/operators'
import { OrgInfoAppService } from 'src/app/_services/org-info-app.service'
import { IResponse } from 'src/app/_interfaces/response.interface'
import { IServiceItem } from 'src/app/_interfaces/service.interface'
import { PopupTextValidateComponent } from 'src/app/_shared/components/popup-text-validate/popup-text-validate.component'
import { MessageService } from '../../../../_shared/services/message.service'
import { UsuarioModel } from 'src/app/_models/usuario.model'
import { SecurityService } from 'src/app/_services/security.service'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { BaseMessageComponent } from '../../../../_shared/components/base-message/base-message.component'

@Component({
  selector: 'app-apps',
  templateUrl: './apps.component.html',
  styleUrls: ['./apps.component.scss']
})
export class AppsComponent extends BaseMessageComponent implements OnInit {
  dataSource: IPagination<IServiceItem>
  page = 1
  count = 10
  word = ''
  isLoading: boolean
  showPage: boolean
  user: UsuarioModel | null

  private _unsubscribeAll: Subject<any>

  constructor(
    private _messageService: MessageService,
    public dialog: MatDialog,
    private _orgInfoAppService: OrgInfoAppService,
    private _busyService: BusyService,
    private _router: Router,
    private _securityService: SecurityService,
    private route: ActivatedRoute
  ) {
    super(route)
    this._unsubscribeAll = new Subject()
  }

  ngOnInit() {
    this.listenLoadingEvent()
    this.getDataSource()
    this.getUser()
    super.ngOnInit()
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  buscar(word) {
    if (!this.isLoading) {
      this.page = 1
      this.word = word
      this.getDataSource()
    }
  }

  async getDataSource() {
    const data = await this._orgInfoAppService.getOrganizationApp(this.word, this.page, this.count).toPromise()
    this.dataSource = data
    this.showPage = true
  }

  get pageIndex() {
    return this.page < 0 ? 0 : this.page - 1
  }

  pageEvent(page: any) {
    this.page = page
    this.getDataSource()
  }

  edit(item: IServiceItem) {
    const _id = item._id
    this._router.navigate(['/institution/general/apps/edit', _id])
  }

  detail(item: IServiceItem) {
    const _id = item._id
    const _name = item.name
    // this._router.navigate(['/application/list/detail', _id, {display: _name}])
    this._router.navigate(['/application/list/detail', _id], { state: { breadcrumb: _name } })
  }

  getRowNumber(index: number) {
    return this.page == 1 ? index + 1 : 1 + index + (this.page - 1) * this.count
  }

  delete(item: IServiceItem) {
    const dialogRef = this.dialog.open(PopupTextValidateComponent, {
      data: {
        title: `Eliminar aplicación`,
        question: `¿Estás seguro que deseas eliminar la aplicación?`,
        confirmQuestion: `Para confirmar, ingresa el nombre de la aplicación`,
        field: `Nombre de la aplicación`,
        fieldData: item.name
      },
      width: '100%',
      maxWidth: '550px',
      panelClass: 'custom-modalbox'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteApp(item._id)
      }
    })
  }

  private async deleteApp(id: string) {
    const response = await this._orgInfoAppService.deleteOrgAppDelete(id).toPromise()

    if (response?.success) {
      this.setMessage(SUCCESS_MESSAGE, 'Registro eliminado con éxito.', true)
      this.getDataSource()
    } else if (response?.error) {
      this.setMessage(ERROR_MESSAGE, response?.error.message, true)
    }
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  private getUser() {
    this._securityService.currentUser$.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.user = x
    })
  }
}

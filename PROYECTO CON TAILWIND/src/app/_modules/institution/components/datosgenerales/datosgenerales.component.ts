import { Component, OnInit } from '@angular/core'
import { Subject } from 'rxjs'
import { BusyService } from 'src/app/_services/busy.service'
import { takeUntil } from 'rxjs/operators'
import { InstitutionService } from 'src/app/_services/institution.service'
import { IOrganizationApplication } from 'src/app/_interfaces/institution.interface'
import { IResponse } from 'src/app/_interfaces/response.interface'
import { MenuItem, MenuNavigation } from 'src/app/_core/components/layout/menu.data'
import { SecurityService } from 'src/app/_services/security.service'
@Component({
  selector: 'app-datosgenerales',
  templateUrl: './datosgenerales.component.html',
  styleUrls: ['./datosgenerales.component.scss']
})
export class DatosgeneralesComponent implements OnInit {
  isLoading: boolean
  showPage: boolean
  private _unsubscribeAll: Subject<any>
  data: IResponse<IOrganizationApplication>
  dataNavigation: MenuItem[]

  constructor(
    private _busyService: BusyService,
    private _institutionService: InstitutionService,
    private securityService: SecurityService
  ) {
    this._unsubscribeAll = new Subject()
  }

  ngOnInit() {
    this.loadMenuData()
    this.listenLoadingEvent()
    this.getInstitution()
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  loadMenuData() {
    this.securityService.currentUser$.pipe(takeUntil(this._unsubscribeAll)).subscribe(user => {
      this.setDataNavigation(user)
    })
  }

  private async getInstitution() {
    const response = await this._institutionService.getOrganizationInfo().toPromise()
    if (response.success) {
      this.data = response
    }
    this.showPage = true
  }

  setDataNavigation(user: any) {
    this.dataNavigation = []
    const adminData = MenuNavigation.filter(item => item.id.includes('admin'))
    const instData = MenuNavigation.filter(item => item.id.includes('institution') && !item.id.includes('admin'))
    if (user.perfil.includes('ADMIN')) {
      this.dataNavigation.push(...adminData)
    }
    if (user.perfil.includes('INSTITUTION-USER')) {
      this.dataNavigation.push(...instData)
    }
  }
}

import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { MatDialog } from '@angular/material/dialog'
import { Subject } from 'rxjs'
import { IPagination } from 'src/app/_interfaces/pagination.interface'
import { BusyService } from 'src/app/_services/busy.service'
import { ActivatedRoute, Router } from '@angular/router'
import { map, takeUntil } from 'rxjs/operators'
import { IResponse } from 'src/app/_interfaces/response.interface'
import { IDeveloperItem } from 'src/app/_interfaces/developer.interface'
import { DevelopersService } from 'src/app/_services/developers.service'
import { PopupTextValidateComponent } from 'src/app/_shared/components/popup-text-validate/popup-text-validate.component'
import { MessageService } from 'src/app/_shared/services/message.service'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { BaseMessageComponent } from 'src/app/_shared/components/base-message/base-message.component'
@Component({
  selector: 'app-developer',
  templateUrl: './developer.component.html',
  styleUrls: ['./developer.component.scss']
})
export class DeveloperComponent extends BaseMessageComponent implements OnInit {
  showPage: boolean

  private _unsubscribeAll: Subject<any>
  dataSource: IPagination<IDeveloperItem>
  page = 1
  count = 10
  word = ''
  formGroup: FormGroup | null
  isLoading = false

  constructor(
    private _messageService: MessageService,
    private _busyService: BusyService,
    private _developerService: DevelopersService,
    private _formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute
  ) {
    super(activatedRoute)
    this._unsubscribeAll = new Subject()
  }

  ngOnInit(): void {
    this.createFormGroup()
    this.listenLoadingEvent()
    this.buscar(this.word)
    super.ngOnInit()
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  delete(item: IDeveloperItem) {
    const _nameArray = [item.names, item.lastname_1, item.lastname_2, item.lastname_3]
    const fullName = _nameArray.join(' ')

    const dialogRef = this.dialog.open(PopupTextValidateComponent, {
      data: {
        title: `Eliminar desarrollador`,
        question: `¿Estás seguro que deseas eliminar al desarrollador?`,
        confirmQuestion: `Para poder eliminarlo, ingresa el nombre del desarrollador`,
        field: `Nombre`,
        fieldData: fullName
      },
      width: '100%',
      maxWidth: '550px',
      panelClass: 'custom-modalbox'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteDev(item._id)
      }
    })
  }

  private async deleteDev(id: string) {
    const response = await this._developerService.deleteOrgDeveloperDelete(id).toPromise()

    if (response?.success) {
      this.renderResponse(response, 'delete')
      this.getDataSource()
    }
  }

  private renderResponse(response: IResponse<any>, method?: string) {
    const delMessage = 'Registro eliminado con éxito.'
    const successMessage = 'Registro guardado con éxito.'
    let message = method === 'delete' ? delMessage : successMessage

    if (response?.success) {
      this.setMessage(SUCCESS_MESSAGE, message, true)
    } else if (response?.error) {
      this.setMessage(ERROR_MESSAGE, response?.error.message, true)
    }
  }

  edit(item: IDeveloperItem) {
    const _id = item._id
    this.router.navigate(['/institution/general/developer/edit', _id])
  }

  getRowNumber(index: number) {
    return this.page == 1 ? index + 1 : 1 + index + (this.page - 1) * this.count
  }

  buscar(word) {
    this.page = 1
    this.word = word
    this.getDataSource()
  }

  private createFormGroup() {
    this.formGroup = this._formBuilder.group({
      filter: [null]
    })
  }

  private async getDataSource() {
    const data = await this._developerService.getOrgDeveloper(this.word, this.page, this.count).toPromise()
    this.showPage = true
    this.dataSource = data
  }

  get pageIndex() {
    return this.page < 0 ? 0 : this.page - 1
  }

  pageEvent(page: any) {
    this.page = page
    this.getDataSource()
  }
}

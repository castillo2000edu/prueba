import { Component, OnInit } from '@angular/core'
import { NewDeveloperComponent } from '../new-developer/new-developer.component'

@Component({
  selector: 'app-edit-developer',
  templateUrl: './edit-developer.component.html',
  styleUrls: ['./edit-developer.component.scss']
})
export class EditDeveloperComponent extends NewDeveloperComponent implements OnInit {
  ngOnInit() {
    super.ngOnInit()
  }
}

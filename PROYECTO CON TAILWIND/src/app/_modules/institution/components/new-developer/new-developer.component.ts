import { Component, Input, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { takeUntil } from 'rxjs/operators'
import { Subject } from 'rxjs'
import { JwtHelperService } from '@auth0/angular-jwt'
import { RegexPatterns } from 'src/app/_shared/regexPatterns'
import { BusyService } from 'src/app/_services/busy.service'
import {
  IDeveloperOne,
  IDeveloperInsertRequest,
  IDeveloperUpdateRequest
} from 'src/app/_interfaces/developer.interface'
import { DevelopersService } from 'src/app/_services/developers.service'
import { MessageService } from '../../../../_shared/services/message.service'
import { TranslateService } from '@ngx-translate/core'
import { getTranslationDeclStmts } from '@angular/compiler/src/render3/view/template'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { BaseMessageComponent } from '../../../../_shared/components/base-message/base-message.component'

@Component({
  selector: 'app-new-developer',
  templateUrl: './new-developer.component.html',
  styleUrls: ['./new-developer.component.scss']
})
export class NewDeveloperComponent extends BaseMessageComponent implements OnInit {
  formGroup: FormGroup | null
  private token: string | null
  private _unsubscribeAll: Subject<any>
  isLoading: boolean
  isSaving: boolean
  showPage: boolean

  @Input() isEditForm: boolean = false
  private developerId: string | null
  private developerItem: IDeveloperOne | null

  public activeLang = 'es'

  constructor(
    private _messageService: MessageService,
    private _busyService: BusyService,
    private router: Router,
    private _formBuilder: FormBuilder,
    private _developerService: DevelopersService,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService
  ) {
    super(activatedRoute)
    this._unsubscribeAll = new Subject()
    this.translate.setDefaultLang(this.activeLang)
    this.translate.use(this.activeLang)
  }

  ngOnInit(): void {
    this.listenLoadingEvent()
    this.createForm()
    if (this.isEditForm) {
      this.getDeveloper()
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  isInValidField(strField: string): boolean {
    const field = this.formGroup.get(strField)
    return field.invalid && field.touched
  }

  getError(strField: string): string {
    const field = this.formGroup.get(strField)
    let messageError = ''
    if (field.errors) {
      if (field.errors.hasOwnProperty('required') && field.errors.required) {
        messageError = `Ingresar ${this.getTranslate(strField)} `
      } else {
        messageError =
          strField == 'filter'
            ? `Ingresar y buscar ${this.getTranslate(strField)}`
            : `Ingresar ${this.getTranslate(strField)} válido`
      }
    }
    return messageError
  }

  getTranslate(word: string): string {
    let translateWord
    this.translate.get(word).subscribe((res: string) => {
      translateWord = res
    })
    return translateWord
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  async save() {
    if (this.isEditForm) {
      return this.update()
    }
    if (this.formGroup.valid) {
      const _formData: { phone; email; position } = this.formGroup.value
      const _request: IDeveloperInsertRequest = {
        token: this.token,
        phone: _formData.phone,
        email: _formData.email,
        position: _formData.position
      }
      this.isSaving = true

      const _response = await this._developerService.postOrgDeveloperInsert(_request).toPromise()

      if (_response.success) {
        this.isSaving = false
        this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', false)
        this.cancel()
      } else if (_response?.error) {
        this.setMessage(ERROR_MESSAGE, _response?.error.message, true)
      }
    }
  }

  private createForm() {
    this.formGroup = this._formBuilder.group({
      filter: [null, [Validators.required, Validators.pattern(RegexPatterns.DNI)]],
      token: this.isEditForm ? [null] : [null, [Validators.required]],
      name: [null],
      lastName: [null],
      phone: [null, [Validators.required, Validators.maxLength(9), Validators.pattern(RegexPatterns.CELULAR)]],
      email: [null, [Validators.required, Validators.pattern(RegexPatterns.EMAIL)]],
      position: [null, [Validators.required, Validators.maxLength(200)]]
    })

    this.formGroup.controls['name'].disable()
    this.formGroup.controls['lastName'].disable()
    this.showPage = true
  }

  async buscar() {
    if (this.isLoading) {
      return false
    }

    if (this.isInValidField('filter')) {
      return false
    }

    const filter = this.formGroup.controls['filter'].value
    const _filter: string = filter?.trim()

    if (_filter == null || _filter?.length < 8) {
      this.formGroup.get('token').setValue(null)
      this.setMessage(ERROR_MESSAGE, 'Debe de ingresar un DNI válido.', true)
      return false
    }

    const response = await this._developerService.getOrgDeveloperSearch(filter).toPromise()

    if (response?.success) {
      this.token = response.token
      const helper = new JwtHelperService()
      const personItem: { name; lastname_1; lastname_2; lastname_3 } = helper.decodeToken(response.token)
      const _lastNameArray = [personItem.lastname_1, personItem.lastname_2, personItem.lastname_3]
      const lastName = _lastNameArray.join(' ')

      const extraData = response.data ?? null
      const phone = extraData?.phone ?? null
      const email = extraData?.email ?? null
      const position = extraData?.position ?? null

      this.formGroup.get('phone').setValue(phone)
      this.formGroup.get('email').setValue(email)
      this.formGroup.get('position').setValue(position)

      this.formGroup.get('name').setValue(personItem.name)
      this.formGroup.get('lastName').setValue(lastName?.trim())
      this.formGroup.get('token').setValue(this.token)
    } else if (response?.error) {
      this.setMessage(ERROR_MESSAGE, response?.error.message, true)
    } else {
      this.setMessage(ERROR_MESSAGE, 'Debe de ingresar un DNI válido.', true)
    }
  }

  cancel() {
    const message = this.textMessage
    const typeMessage = this.typeMessage
    this.router.navigate(['/institution/general/developer'], { state: { message, typeMessage } })
  }

  private async getDeveloper() {
    this.developerId = this.activatedRoute.snapshot.paramMap.get('id') ?? null
    if (!this.developerId) {
      this.router.navigate(['/institution/general/developer'])
      return false
    }
    const response = await this._developerService.getOrgDeveloperOne(this.developerId).toPromise()
    this.showPage = true
    if (response.success) {
      this.developerItem = response.Item
      const _lastName = `${this.developerItem.lastname_1} ${this.developerItem.lastname_2} ${this.developerItem.lastname_3}`
      this.formGroup.get('filter').setValue(this.developerItem.doc)
      this.formGroup.get('name').setValue(this.developerItem.names)
      this.formGroup.get('lastName').setValue(_lastName)
      this.formGroup.get('phone').setValue(this.developerItem.phone)
      this.formGroup.get('email').setValue(this.developerItem.email)
      this.formGroup.get('position').setValue(this.developerItem.position)
    } else {
      this.router.navigate(['/institution/general/developer'])
    }
  }

  private async update() {
    if (this.developerItem) {
      const request: IDeveloperUpdateRequest = {
        id: this.developerId,
        email: this.formGroup.value.email,
        phone: this.formGroup.value.phone,
        position: this.formGroup.value.position
      }

      this.isSaving = true
      const _response = await this._developerService.putOrgDeveloperUpdate(request).toPromise()
      if (_response.success) {
        this.isSaving = false
        this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', false)
        this.cancel()
      } else if (_response?.error) {
        this.setMessage(ERROR_MESSAGE, _response?.error.message, true)
      }
    }
  }

  setTitle() {
    return `${this.isEditForm ? 'Editar' : 'Nuevo'} Desarrollador`
  }

  setSubtitle() {
    return `Completa los campos para ${this.isEditForm ? 'editar el desarrollador' : 'crear un nuevo desarrollador'}`
  }
}

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { InstitutionRoutingModule } from './institution-routing.module'
import { AppsComponent } from './components/apps/apps.component'
import { DatosgeneralesComponent } from './components/datosgenerales/datosgenerales.component'
import { DeveloperComponent } from './components/developer/developer.component'
import { EditDeveloperComponent } from './components/edit-developer/edit-developer.component'
import { EditarAppComponent } from './components/editar-app/editar-app.component'
import { NewDeveloperComponent } from './components/new-developer/new-developer.component'
import { MaterialModule } from 'src/app/material/material.module'
import { ReactiveFormsModule } from '@angular/forms'
import { SharedModule } from '../../_shared/shared.module'
import { HelperModule } from 'src/app/_helpers/helper.module'
import { CoreModule } from 'src/app/_core/core.module'
import { NgxPaginationModule } from 'ngx-pagination'
import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { AuthInterceptor } from 'src/app/_interceptors/auth.interceptor'
import { BaseMessageComponent } from '../../_shared/components/base-message/base-message.component'

@NgModule({
  declarations: [
    AppsComponent,
    DatosgeneralesComponent,
    DeveloperComponent,
    EditDeveloperComponent,
    EditarAppComponent,
    NewDeveloperComponent,
    BaseMessageComponent
  ],
  imports: [
    InstitutionRoutingModule,
    NgxPaginationModule,
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    HelperModule,
    CoreModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }]
})
export class InstitutionModule {}

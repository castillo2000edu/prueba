import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { AplicacionesComponent } from './components/aplicaciones/aplicaciones.component'
import { NewApplicationComponent } from './components/new-application/new-application.component'
import { CoreModule } from 'src/app/_core/core.module'
import { MaterialModule } from 'src/app/material/material.module'
import { ReactiveFormsModule } from '@angular/forms'

import { SharedModule } from 'src/app/_shared/shared.module'
import { NgxPaginationModule } from 'ngx-pagination'
import { HelperModule } from 'src/app/_helpers/helper.module'
import { ApplicationRoutingModule } from './application-routing.module'
import { AppDetailComponent } from './components/app-detail/app-detail.component'
import { DigitalAuthenticationComponent } from './components/digital-authentication/digital-authentication.component'
import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { AuthInterceptor } from 'src/app/_interceptors/auth.interceptor'
import { BaseComponent } from './components/base/base.component'
import { CredentialPanelComponent } from './components/credential-panel/credential-panel.component'
import { NewCredentialPanelComponent } from './components/new-credential-panel/new-credential-panel.component'
import { DisableServiceComponent } from './components/disable-service/disable-service.component'
import { EnableServiceComponent } from './components/enable-service/enable-service.component'
import { DigitalSignatureComponent } from './components/digital-signature/digital-signature.component'
import { AutomationAgentComponent } from './components/automation-agent/automation-agent.component'
import { SignatureValidationComponent } from './components/signature-validation/signature-validation.component'
import { NewVersionComponent } from './components/new-version/new-version.component'
import { EditVersionComponent } from './components/edit-version/edit-version.component'

@NgModule({
  declarations: [
    AplicacionesComponent,
    NewApplicationComponent,
    AppDetailComponent,
    DigitalAuthenticationComponent,
    BaseComponent,
    CredentialPanelComponent,
    NewCredentialPanelComponent,
    DisableServiceComponent,
    EnableServiceComponent,
    DigitalSignatureComponent,
    AutomationAgentComponent,
    SignatureValidationComponent,
    NewVersionComponent,
    EditVersionComponent
  ],
  imports: [
    ApplicationRoutingModule,
    NgxPaginationModule,
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    HelperModule,
    CoreModule
  ],
  exports: [CredentialPanelComponent, NewCredentialPanelComponent],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }]
})
export class ApplicationModule {}

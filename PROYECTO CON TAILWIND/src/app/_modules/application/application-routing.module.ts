import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { NewApplicationComponent } from './components/new-application/new-application.component'
import { ChildDynamicBreadcrumbResolver } from 'src/app/_resolvers/child-dynamic-breadcrumb.resolver'
import { DynamicNameBreadCrumbResolver } from 'src/app/_resolvers/dynamic-name-bread-crumb.resolver'
import { AppsComponent } from '../institution/components/apps/apps.component'
import { AppDetailComponent } from './components/app-detail/app-detail.component'
import { DigitalAuthenticationComponent } from './components/digital-authentication/digital-authentication.component'
import { DigitalSignatureComponent } from './components/digital-signature/digital-signature.component'
import { AutomationAgentComponent } from './components/automation-agent/automation-agent.component'
import { SignatureValidationComponent } from './components/signature-validation/signature-validation.component'

const routes: Routes = [
  {
    path: 'list',
    data: { breadcrumb: 'Aplicaciones', roles: ['APP-USER'] },
    children: [
      { path: '', component: AppsComponent, data: { breadcrumb: 'Aplicaciones', roles: ['APP-USER'] } },
      { path: 'new', data: { breadcrumb: 'Nueva aplicación' }, component: NewApplicationComponent },
      {
        path: 'detail/:id',
        resolve: { breadcrumb: DynamicNameBreadCrumbResolver },
        children: [
          { path: '', component: AppDetailComponent },
          {
            path: 'digital-authentication',
            data: { breadcrumb: 'Autentificación digital' },
            resolve: { breadcrumb: ChildDynamicBreadcrumbResolver },
            component: DigitalAuthenticationComponent
          },
          {
            path: 'digital-signature',
            data: { breadcrumb: 'Firma digital' },
            resolve: { breadcrumb: ChildDynamicBreadcrumbResolver },
            component: DigitalSignatureComponent
          },
          {
            path: 'automation-agent',
            data: { breadcrumb: 'Agente automatizado' },
            resolve: { breadcrumb: ChildDynamicBreadcrumbResolver },
            component: AutomationAgentComponent
          },
          {
            path: 'signature-validation',
            data: { breadcrumb: 'Validacion firma' },
            resolve: { breadcrumb: ChildDynamicBreadcrumbResolver },
            component: SignatureValidationComponent
          }
        ]
      }
    ]
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationRoutingModule {}

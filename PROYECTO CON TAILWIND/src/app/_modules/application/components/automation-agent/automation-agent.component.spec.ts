import { ComponentFixture, TestBed } from '@angular/core/testing'

import { AutomationAgentComponent } from './automation-agent.component'

describe('AutomationAgentComponent', () => {
  let component: AutomationAgentComponent
  let fixture: ComponentFixture<AutomationAgentComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AutomationAgentComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomationAgentComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

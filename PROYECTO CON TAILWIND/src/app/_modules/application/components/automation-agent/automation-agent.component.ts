import { Component, OnInit } from '@angular/core'
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { forkJoin, Subject } from 'rxjs'
import { map, takeUntil } from 'rxjs/operators'
import { IAppOneResponse, IServiceOneResponse, IServiceUpdateRequest } from 'src/app/_interfaces/application.interface'
import { ApplicationService } from 'src/app/_services/application.service'
import { BusyService } from 'src/app/_services/busy.service'
import { BaseMessageComponent } from 'src/app/_shared/components/base-message/base-message.component'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { RegexPatterns } from 'src/app/_shared/regexPatterns'
import { MessageService } from 'src/app/_shared/services/message.service'

@Component({
  selector: 'app-automation-agent',
  templateUrl: './automation-agent.component.html',
  styleUrls: ['./automation-agent.component.scss']
})
export class AutomationAgentComponent extends BaseMessageComponent implements OnInit {
  private _unsubscribeAll: Subject<any>

  isLoading: boolean
  enabled: boolean = false
  showPage: boolean

  currentApp: IAppOneResponse | null
  currentService: IServiceOneResponse | null

  configForm: FormGroup | null
  private appId: string | null

  constructor(
    private translate: TranslateService,
    private _busyService: BusyService,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public appService: ApplicationService,
    private _formBuilder: FormBuilder,
    private _messageService: MessageService
  ) {
    super(activatedRoute)
    this._unsubscribeAll = new Subject()
    this.translate.setDefaultLang('es')
    this.translate.use('es')
  }

  ngOnInit(): void {
    this.listenLoadingEvent()
    this.getAppId()
    this.createForm()
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  private async getAppId() {
    const appId = this.activatedRoute.snapshot.paramMap.get('id') ?? null
    if (appId) {
      await this.setValidApplication(appId)
    } else {
      this.router.navigate(['/'])
    }
  }

  private async setValidApplication(id: string) {
    this.appService.setServicesSource(null)
    let response = false
    response = await forkJoin([
      this.appService.getApplicationAppOne(id),
      this.appService.getApplicationOrganizationServices(id)
    ])
      .pipe(takeUntil(this._unsubscribeAll))
      .pipe(
        map(([appResponse, servicesResponse]) => {
          let response: boolean = false
          if (appResponse?.success) {
            this.appId = appResponse.Item._id
            this.appService.setAppCurrentId(this.appId)
            this.appService.setCurrentApp(appResponse.Item)
            response = true
            this.getApplicationData()
          } else {
            return false
          }
          if (this.appId && servicesResponse?.success) {
            this.appService.setServicesSource(servicesResponse.service)
          }
          return response
        })
      )
      .toPromise()
    if (!response) {
      this.router.navigate(['/'])
    }
  }

  private async getApplicationData() {
    this.appService
      .getCurrentApp()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(app => {
        if (app) {
          this.currentApp = app
          this.getServiceOne()
        }
      })
  }

  private async getServiceOne() {
    if (this.currentApp) {
      const clientId = this.currentApp?.client_id
      const response = await this.appService.getApplicationServiceOne(clientId, 'sign_agent').toPromise()
      this.showPage = true
      if (response?.success) {
        this.enabled = true
        this.currentService = response.Item
        this.setFormConfigData()
      }
    }
  }

  private setFormConfigData() {
    if (this.currentService) {
      const _macs = this.currentService.mac_authorized || []
      this.macsFormArray.clear()
      _macs.forEach(mac => {
        this.macsFormArray.push(this.newMacFormGroup(mac, true))
      })
      if (!_macs.length) {
        this.macsFormArray.push(this.newMacFormGroup(null, false))
      }
      this.configForm.markAsPristine()
    }
  }

  get macsFormArray(): FormArray {
    return this.configForm.get('_macs') as FormArray
  }

  private newMacFormGroup(uri: string, isTouched: boolean) {
    return this._formBuilder.group({
      mac: [uri, [Validators.pattern(RegexPatterns.MAC)]]
    })
  }

  createForm() {
    this.configForm = this._formBuilder.group({
      _macs: this._formBuilder.array([this.newMacFormGroup(null, false)])
    })
  }

  addMac(arrayName: string) {
    if (this.getformArray(arrayName).length < 20) {
      const array = this.configForm.controls[arrayName] as FormArray
      array.push(this.newMacFormGroup(null, false))
    }
  }

  getformArray(arrayName: string) {
    return (this.configForm.controls[arrayName] as FormArray).controls
  }

  removeMac(arrayName: string, i: number) {
    const array = this.configForm.controls[arrayName] as FormArray
    array.removeAt(i)
    this.configForm.markAsDirty()
  }

  showError(control: FormControl) {
    return control.invalid && control.touched
  }

  getErrorMessage(control: FormControl, strField: string) {
    const field = control.get('mac')
    let messageError = ''
    if (field.errors) {
      if (field.errors.hasOwnProperty('required') && field.errors.required) {
        messageError = `Ingresar ${this.getTranslate(strField)} `
      } else {
        messageError =
          strField == 'filter'
            ? `Ingresar y buscar ${this.getTranslate(strField)}`
            : `Ingresar ${this.getTranslate(strField)} válido`
      }
    }
    return messageError
  }

  getTranslate(word: string): string {
    let translateWord
    this.translate.get(word).subscribe((res: string) => {
      translateWord = res
    })
    return translateWord
  }

  disableEvent(response: any) {
    if (response.success) {
      this.enabled = !response.success
      this.setMessage(SUCCESS_MESSAGE, response.messageText, true)
    } else {
      this.setMessage(ERROR_MESSAGE, response.messageText, true)
    }
  }

  enableEvent(response: any) {
    if (response.success) {
      this.enabled = response.success
      this.setMessage(SUCCESS_MESSAGE, response.messageText, true)
      this.getApplicationData()
    } else {
      this.setMessage(ERROR_MESSAGE, response.messageText, true)
    }
  }

  newCredentialEvent(response: any) {
    if (response.success) {
      this.setMessage(SUCCESS_MESSAGE, response.messageText, true)
      this.getApplicationData()
    } else {
      this.setMessage(ERROR_MESSAGE, response.messageText, true)
    }
  }

  setMacId(type: string, num: number) {
    return `${type}MAC${num}`
  }

  setMacLabel(type: string, num: number) {
    return num >= 1 ? `MAC de ${type} - ${num + 1}` : `MAC de ${type}`
  }

  async save() {
    if (this.configForm.invalid) {
      return false
    }
    const formValue = this.configForm.value
    const id = this.currentService._id
    const service_code = this.currentService.service_code
    const mac_authorized = (formValue._macs as [])
      .filter((x: { mac }) => x.mac !== null)
      .map((x: { mac }) => x.mac.trim())
      .join(',')
    const request: IServiceUpdateRequest = { id, service_code, mac_authorized }
    const response = await this.appService.putApplicationServiceUpdate(request).toPromise()
    if (response?.success) {
      this.configForm.markAsPristine()
      this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', true)
    } else if (response?.error) {
      const error = response?.error
      this.setMessage(ERROR_MESSAGE, error.message, true)
    }
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { TranslateService } from '@ngx-translate/core'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { ApplicationService } from 'src/app/_services/application.service'
import { BusyService } from 'src/app/_services/busy.service'
import { PopupTextValidateComponent } from 'src/app/_shared/components/popup-text-validate/popup-text-validate.component'
import { MessageService } from 'src/app/_shared/services/message.service'

@Component({
  selector: 'app-disable-service',
  templateUrl: './disable-service.component.html',
  styleUrls: ['./disable-service.component.scss']
})
export class DisableServiceComponent implements OnInit {
  @Input()
  serviceId: string | null

  @Input()
  serviceName: string | null

  @Output()
  disableEvent: EventEmitter<any> | null

  @Input()
  serviceDescription: string | null

  isLoading: boolean

  private _unsubscribeAll: Subject<any>

  constructor(
    private _busyService: BusyService,
    private _messageService: MessageService,
    private dialog: MatDialog,
    private _srvService: ApplicationService,
    private translate: TranslateService
  ) {
    this.disableEvent = new EventEmitter<any>()
    this._unsubscribeAll = new Subject()
    this.translate.setDefaultLang('es')
    this.translate.use('es')
  }

  ngOnInit(): void {
    this.listenLoadingEvent()
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  disableApp() {
    if (this.serviceId) {
      const dialogRef = this.dialog.open(PopupTextValidateComponent, {
        data: {
          title: `Deshabilitar el servicio`,
          question: `¿Estás seguro que deseas deshabilitar el servicio?`,
          confirmQuestion: `Para confirmar, ingresa el nombre del servicio`,
          field: `Nombre del servicio`,
          fieldData: this.getTranslate(this.serviceName),
          confirmBtnText: `Confirmar y deshabilitar`
        },
        width: '100%',
        maxWidth: '550px',
        panelClass: 'custom-modalbox'
      })
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.disableService()
        }
      })
    }
  }

  getTranslate(word: string): string {
    let translateWord
    this.translate.get(word).subscribe((res: string) => {
      translateWord = res
    })
    return translateWord
  }

  private async disableService() {
    const response = await this._srvService.putApplicationServiceDisabled(this.serviceId).toPromise()

    if (response.success) {
      this.disableEvent.emit({ success: true, messageText: 'Se deshabilitó correctamente.' })
    } else if (response?.error) {
      const error = response?.error
      this.disableEvent.emit({ success: false, messageText: error.message })
    }
  }
}

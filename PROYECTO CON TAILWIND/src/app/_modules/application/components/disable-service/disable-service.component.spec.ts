import { ComponentFixture, TestBed } from '@angular/core/testing'

import { DisableServiceComponent } from './disable-service.component'

describe('DisableServiceComponent', () => {
  let component: DisableServiceComponent
  let fixture: ComponentFixture<DisableServiceComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DisableServiceComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(DisableServiceComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

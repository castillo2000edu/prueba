import { ComponentFixture, TestBed } from '@angular/core/testing'

import { CredentialPanelComponent } from './credential-panel.component'

describe('CredentialPanelComponent', () => {
  let component: CredentialPanelComponent
  let fixture: ComponentFixture<CredentialPanelComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CredentialPanelComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(CredentialPanelComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

import { Component, Input, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { IVersionRequest, IVersionResponse } from 'src/app/_interfaces/version.interface'
import { BusyService } from 'src/app/_services/busy.service'
import { VersionService } from 'src/app/_services/version.service'
import { BaseMessageComponent } from 'src/app/_shared/components/base-message/base-message.component'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { MessageService } from 'src/app/_shared/services/message.service'

@Component({
  selector: 'app-new-version',
  templateUrl: './new-version.component.html',
  styleUrls: ['./new-version.component.scss']
})
export class NewVersionComponent extends BaseMessageComponent implements OnInit {
  private _unsubscribeAll: Subject<any>
  isLoading: boolean
  @Input() isEditForm: boolean = false
  formGroup: FormGroup | null
  versionId: string
  serviceCode: string
  showPage: boolean
  versionItem: IVersionResponse

  constructor(
    private _busyService: BusyService,
    private translate: TranslateService,
    private _formBuilder: FormBuilder,
    private _versionService: VersionService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _messageService: MessageService
  ) {
    super(activatedRoute)
    this._unsubscribeAll = new Subject()
    this.translate.setDefaultLang('es')
    this.translate.use('es')
  }

  ngOnInit(): void {
    this.versionId = this.activatedRoute.snapshot.paramMap.get('id') ?? null
    this.serviceCode = this.activatedRoute.snapshot.paramMap.get('code') ?? null
    this.listenLoadingEvent()
    this.createForm()
    if (this.isEditForm) {
      this.getVersion()
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  private createForm() {
    this.formGroup = this._formBuilder.group({
      version_number: [null, [Validators.required, Validators.maxLength(20)]]
    })
    if (!this.isEditForm) {
      this.showPage = true
    }
  }

  private async getVersion() {
    if (!this.versionId) {
      this.cancel()
      return false
    }
    const response = await this._versionService.getAdminVersionOne(this.versionId).toPromise()
    if (response.success) {
      this.versionItem = response.Item
      this.formGroup.get('version_number').setValue(this.versionItem.version_number)
    } else {
      this.setMessage(ERROR_MESSAGE, response?.error.message, true)
    }
    this.showPage = true
  }

  isInValidField(strField: string): boolean {
    const field = this.formGroup.get(strField)
    return field.invalid && field.touched
  }

  getError(strField: string): string {
    const field = this.formGroup.get(strField)
    let messageError = ''
    if (field.errors) {
      if (field.errors.hasOwnProperty('required') && field.errors.required) {
        messageError = `Ingresar ${this.getTranslate(strField)} `
      } else {
        messageError =
          strField == 'filter'
            ? `Ingresar y buscar ${this.getTranslate(strField)}`
            : `Ingresar ${this.getTranslate(strField)} válido`
      }
    }
    return messageError
  }

  getTranslate(word: string): string {
    let translateWord
    this.translate.get(word).subscribe((res: string) => {
      translateWord = res
    })
    return translateWord
  }

  setTitle() {
    return `${this.isEditForm ? 'Editar' : 'Nueva'} versión`
  }

  cancel() {
    const message = this.textMessage
    const typeMessage = this.typeMessage
    this.router.navigate(['/institution/general/services', this.serviceCode, 'versions'], {
      state: { message, typeMessage }
    })
  }

  async save() {
    if (this.isEditForm) {
      return this.update()
    }
    if (this.formGroup.valid) {
      const _formData: { version_number } = this.formGroup.value
      const request: IVersionRequest = { service_code: this.serviceCode, version_number: _formData.version_number }
      const response = await this._versionService.postAdminVersionInsert(request).toPromise()
      if (response.success) {
        this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', false)
        this.cancel()
      } else if (response?.error) {
        this.setMessage(ERROR_MESSAGE, response?.error.message, true)
      }
    }
  }

  private async update() {
    if (this.formGroup.valid) {
      const _formData: { version_number } = this.formGroup.value
      const id = this.versionId
      const version_number = _formData.version_number
      const request: IVersionRequest = { id, version_number }
      const response = await this._versionService.putAdminVersionUpdate(request).toPromise()
      if (response.success) {
        this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', false)
        this.cancel()
      } else if (response?.error) {
        this.setMessage(ERROR_MESSAGE, response?.error.message, true)
      }
    }
  }
}

import { DatePipe } from '@angular/common'
import { Component, OnInit } from '@angular/core'
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { Base64 } from 'js-base64'
import { forkJoin, Subject } from 'rxjs'
import { map, takeUntil } from 'rxjs/operators'
import { IAppUpdateRequest } from 'src/app/_interfaces/application.interface'
import { ApplicationService } from 'src/app/_services/application.service'
import { BusyService } from 'src/app/_services/busy.service'
import { BaseMessageComponent } from 'src/app/_shared/components/base-message/base-message.component'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { RegexPatterns } from 'src/app/_shared/regexPatterns'
import { MessageService } from 'src/app/_shared/services/message.service'

@Component({
  selector: 'app-app-detail',
  templateUrl: './app-detail.component.html',
  styleUrls: ['./app-detail.component.scss'],
  providers: [DatePipe]
})
export class AppDetailComponent extends BaseMessageComponent implements OnInit {
  id: string
  private _unsubscribeAll: Subject<any>

  enabled: boolean
  isLoading: boolean
  showPage: boolean

  appInfoForm: FormGroup | null
  orgInfoForm: FormGroup | null
  devInfoForm: FormGroup | null
  services: any | null

  dataInstitution: Object
  dataApp: Object

  constructor(
    private activatedRoute: ActivatedRoute,
    private _busyService: BusyService,
    public router: Router,
    public appService: ApplicationService,
    private _formBuilder: FormBuilder,
    public datepipe: DatePipe,
    private translate: TranslateService,
    private _messageService: MessageService
  ) {
    super(activatedRoute)
    this._unsubscribeAll = new Subject()
    this.translate.setDefaultLang('es')
    this.translate.use('es')
  }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id')
    this.listenLoadingEvent()
    this.getAppId()
    this.createForm()
  }

  private async getAppId() {
    if (this.id) {
      await this.setValidApplication(this.id)
    } else {
      this.router.navigate(['/'])
    }
  }

  private async setValidApplication(id: string) {
    this.appService.setServicesSource(null)
    let response = false
    response = await forkJoin([
      this.appService.getApplicationAppOne(id),
      this.appService.getApplicationOrganizationServices(id)
    ])
      .pipe(takeUntil(this._unsubscribeAll))
      .pipe(
        map(([appResponse, servicesResponse]) => {
          let response: boolean = false
          if (appResponse?.success) {
            this.id = appResponse.Item._id
            this.appService.setAppCurrentId(this.id)
            this.appService.setCurrentApp(appResponse.Item)
            response = true
            this.getApplicationData()
          } else {
            return false
          }
          if (this.id && servicesResponse?.success) {
            this.appService.setServicesSource(servicesResponse.service)
          }
          return response
        })
      )
      .toPromise()
    if (!response) {
      this.router.navigate(['/'])
    }
  }

  private async getApplicationData() {
    this.appService
      .getAppCurrentId()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(id => {
        if (id) {
          this.getAppInfo(id)
          this.getServiceInfo()
        }
      })
  }

  private async getAppInfo(id: string) {
    const response = await this.appService.getApplicationAppInfo(id).toPromise()
    if (response?.success) {
      this.showPage = true
      const data = response?.data
      if (data?.app) {
        this.enabled = data?.app?.enabled
        this.appInfoForm.patchValue(data?.app)
        this.dataApp = data?.app
      }
      if (data?.organization) {
        this.orgInfoForm.patchValue(data.organization)
        this.dataInstitution = data.organization
        this.domains.clear()
        data.organization.domains.forEach(domain => {
          this.domains.push(this._formBuilder.control({ value: domain, disabled: true }))
        })
      }
      if (data?.developer) {
        const dev = data.developer
        dev.created_at = this.datepipe.transform(dev.created_at, 'dd/MM/yyyy HH:mm')
        this.devInfoForm.patchValue(dev)
      }
    }
  }

  get domains() {
    return this.orgInfoForm.controls['domains'] as FormArray
  }

  private async getServiceInfo() {
    this.appService
      .getServicesSource()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(services => {
        if (services) {
          this.services = services
        }
      })
  }

  createForm() {
    this.appInfoForm = this._formBuilder.group({
      client_id: this._formBuilder.control({ value: null, disabled: true }),
      name: [null, [Validators.required, Validators.maxLength(50)]],
      description: [null, [Validators.required, Validators.maxLength(150)]],
      url: [null, [Validators.required, Validators.pattern(RegexPatterns.URL)]]
    })

    this.orgInfoForm = this._formBuilder.group({
      code: this._formBuilder.control({ value: null, disabled: true }),
      name: this._formBuilder.control({ value: null, disabled: true }),
      domains: this._formBuilder.array([])
    })

    this.devInfoForm = this._formBuilder.group({
      created_at: this._formBuilder.control({ value: null, disabled: true }),
      doc: this._formBuilder.control({ value: null, disabled: true }),
      names: this._formBuilder.control({ value: null, disabled: true }),
      lastname: this._formBuilder.control({ value: null, disabled: true })
    })
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  hasError(strField: string): boolean {
    const field = this.appInfoForm.get(strField)
    return field.invalid && field.touched
  }

  getErrorMessage(strField: string) {
    const field = this.appInfoForm.get(strField)
    let messageError = ''
    if (field.errors) {
      if (field.errors.hasOwnProperty('required') && field.errors.required) {
        messageError = `Ingresar ${this.getTranslate(strField)} `
      } else {
        messageError =
          strField == 'filter'
            ? `Ingresar y buscar ${this.getTranslate(strField)}`
            : `Ingresar ${this.getTranslate(strField)} válido`
      }
    }
    return messageError
  }

  getTranslate(word: string): string {
    let translateWord
    this.translate.get(word).subscribe((res: string) => {
      translateWord = res
    })
    return translateWord
  }

  getLinkService(serviceName: string) {
    switch (serviceName) {
      case 'auth': {
        return 'digital-authentication'
      }
      case 'sign_digital': {
        return 'digital-signature'
      }
      case 'sign_validation': {
        return 'signature-validation'
      }
      case 'sign_agent': {
        return 'automation-agent'
      }
      default: {
        return serviceName
      }
    }
  }

  capitalize(word: string) {
    const lower = word.toLowerCase()
    return word.charAt(0).toUpperCase() + lower.slice(1)
  }

  async save() {
    if (this.appInfoForm.valid) {
      let { name, description, url } = this.appInfoForm.value
      name = Base64.encode(name)
      description = Base64.encode(description)
      const request: IAppUpdateRequest = { id: this.id, name, description, url }
      const response = await this.appService.putApplicationAppUpdate(request).toPromise()
      if (response?.success) {
        this.appInfoForm.markAsPristine()
        this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', true)
      } else if (response?.error) {
        const error = response?.error
        this.setMessage(ERROR_MESSAGE, error.message, true)
      }
    }
  }
}

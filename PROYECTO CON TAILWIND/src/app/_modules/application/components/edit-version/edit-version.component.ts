import { Component, OnInit } from '@angular/core'
import { NewVersionComponent } from '../new-version/new-version.component'

@Component({
  selector: 'app-edit-version',
  templateUrl: './edit-version.component.html',
  styleUrls: ['./edit-version.component.scss']
})
export class EditVersionComponent extends NewVersionComponent implements OnInit {
  ngOnInit(): void {
    super.ngOnInit()
  }
}

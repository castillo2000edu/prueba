import { ComponentFixture, TestBed } from '@angular/core/testing'

import { EnableServiceComponent } from './enable-service.component'

describe('EnableServiceComponent', () => {
  let component: EnableServiceComponent
  let fixture: ComponentFixture<EnableServiceComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EnableServiceComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(EnableServiceComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

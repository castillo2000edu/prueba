import { ComponentFixture, TestBed } from '@angular/core/testing'

import { DigitalAuthenticationComponent } from './digital-authentication.component'

describe('DigitalAuthenticationComponent', () => {
  let component: DigitalAuthenticationComponent
  let fixture: ComponentFixture<DigitalAuthenticationComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DigitalAuthenticationComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitalAuthenticationComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

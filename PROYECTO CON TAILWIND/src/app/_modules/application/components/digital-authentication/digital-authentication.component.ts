import { Component, OnInit } from '@angular/core'
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { forkJoin, Subject } from 'rxjs'
import { map, takeUntil } from 'rxjs/operators'
import { IAppOneResponse, IServiceOneResponse, IServiceUpdateRequest } from 'src/app/_interfaces/application.interface'
import { ApplicationService } from 'src/app/_services/application.service'
import { BusyService } from 'src/app/_services/busy.service'
import { BaseMessageComponent } from 'src/app/_shared/components/base-message/base-message.component'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { RegexPatterns } from 'src/app/_shared/regexPatterns'
import { MessageService } from 'src/app/_shared/services/message.service'

@Component({
  selector: 'app-digital-authentication',
  templateUrl: './digital-authentication.component.html',
  styleUrls: ['./digital-authentication.component.scss']
})
export class DigitalAuthenticationComponent extends BaseMessageComponent implements OnInit {
  private _unsubscribeAll: Subject<any>
  isLoading: boolean
  enabled: boolean
  showPage: boolean
  configForm: FormGroup | null
  private appId: string | null

  currentApp: IAppOneResponse | null
  currentService: IServiceOneResponse | null

  constructor(
    private _formBuilder: FormBuilder,
    private translate: TranslateService,
    private _busyService: BusyService,
    public appService: ApplicationService,
    private _messageService: MessageService,
    public activatedRoute: ActivatedRoute,
    public router: Router
  ) {
    super(activatedRoute)
    this._unsubscribeAll = new Subject()
    this.translate.setDefaultLang('es')
    this.translate.use('es')
  }

  ngOnInit(): void {
    this.listenLoadingEvent()
    this.getAppId()
    this.createForms()
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next()
    this._unsubscribeAll.complete()
  }

  private async getAppId() {
    const appId = this.activatedRoute.snapshot.paramMap.get('id') ?? null
    if (appId) {
      await this.setValidApplication(appId)
    } else {
      this.router.navigate(['/'])
    }
  }

  private async setValidApplication(id: string) {
    this.appService.setServicesSource(null)
    let response = false
    response = await forkJoin([
      this.appService.getApplicationAppOne(id),
      this.appService.getApplicationOrganizationServices(id)
    ])
      .pipe(takeUntil(this._unsubscribeAll))
      .pipe(
        map(([appResponse, servicesResponse]) => {
          let response: boolean = false
          if (appResponse?.success) {
            this.appId = appResponse.Item._id
            this.appService.setAppCurrentId(this.appId)
            this.appService.setCurrentApp(appResponse.Item)
            response = true
            this.getApplicationData()
          } else {
            return false
          }
          if (this.appId && servicesResponse?.success) {
            this.appService.setServicesSource(servicesResponse.service)
          }
          return response
        })
      )
      .toPromise()
    if (!response) {
      this.router.navigate(['/'])
    }
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }
  createForms() {
    this.configForm = this._formBuilder.group({
      tos_url: [null, [Validators.required, Validators.pattern(RegexPatterns.URL)]],
      policy_url: [null, [Validators.required, Validators.pattern(RegexPatterns.URL)]],
      _redirect_uris: this._formBuilder.array([
        this._formBuilder.group({
          uri: [null, [Validators.required, Validators.pattern(RegexPatterns.URL)]]
        })
      ]),
      _js_origin_uris: this._formBuilder.array([
        this._formBuilder.group({
          uri: [null, [Validators.required, , Validators.pattern(RegexPatterns.URL)]]
        })
      ])
    })
  }

  addUri(arrayName: string) {
    if (this.getformArray(arrayName).length < 20) {
      const array = this.configForm.controls[arrayName] as FormArray
      array.push(this.newUriFormGroup(null, false))
    }
  }

  removeUri(arrayName: string, i: number) {
    const array = this.configForm.controls[arrayName] as FormArray
    array.removeAt(i)
    this.configForm.markAsDirty()
  }

  getformArray(arrayName: string) {
    return (this.configForm.controls[arrayName] as FormArray).controls
  }

  private newUriFormGroup(uri: string, isTouched: boolean) {
    return this._formBuilder.group({
      uri: [uri, [Validators.required, Validators.pattern(RegexPatterns.URL)]]
    })
  }

  getError(strField: string): string {
    const field = this.configForm.get(strField)
    let messageError = ''
    if (field.errors) {
      if (field.errors.hasOwnProperty('required') && field.errors.required) {
        messageError = `Ingresar ${this.getTranslate(strField)} `
      } else {
        messageError =
          strField == 'filter'
            ? `Ingresar y buscar ${this.getTranslate(strField)}`
            : `Ingresar ${this.getTranslate(strField)} válido`
      }
    }
    return messageError
  }

  getTranslate(word: string): string {
    let translateWord
    this.translate.get(word).subscribe((res: string) => {
      translateWord = res
    })
    return translateWord
  }

  isUrlError(controlName: string) {
    const field = this.configForm.get(controlName)
    return this.configForm.controls[controlName].invalid && field.touched
  }

  isUrlErrorByCtrl(control: FormControl) {
    return control.invalid && control.touched
  }

  getErrorByCtrl(control: FormControl, strField: string) {
    const field = control.get('uri')
    let messageError = ''
    if (field.errors) {
      if (field.errors.hasOwnProperty('required') && field.errors.required) {
        messageError = `Ingresar ${this.getTranslate(strField)} `
      } else {
        messageError =
          strField == 'filter'
            ? `Ingresar y buscar ${this.getTranslate(strField)}`
            : `Ingresar ${this.getTranslate(strField)} válido`
      }
    }
    return messageError
  }

  disableEvent(response: any) {
    if (response.success) {
      this.enabled = !response.success
      this.setMessage(SUCCESS_MESSAGE, response.messageText, true)
    } else {
      this.setMessage(ERROR_MESSAGE, response.messageText, true)
    }
  }

  enableEvent(response: any) {
    if (response.success) {
      this.enabled = response.success
      this.setMessage(SUCCESS_MESSAGE, response.messageText, true)
      this.getApplicationData()
    } else {
      this.setMessage(ERROR_MESSAGE, response.messageText, true)
    }
  }

  newCredentialEvent(response: any) {
    if (response.success) {
      this.setMessage(SUCCESS_MESSAGE, response.messageText, true)
      this.getApplicationData()
    } else {
      this.setMessage(ERROR_MESSAGE, response.messageText, true)
    }
  }

  private async getApplicationData() {
    this.appService
      .getCurrentApp()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(app => {
        if (app) {
          this.currentApp = app
          this.getServiceOne()
        }
      })
  }

  private async getServiceOne() {
    if (this.currentApp) {
      const clientId = this.currentApp?.client_id
      const response = await this.appService.getApplicationServiceOne(clientId, 'auth').toPromise()
      this.showPage = true
      if (response?.success) {
        this.enabled = true
        this.currentService = response.Item
        this.setFormConfigData()
      }
    }
  }

  private setFormConfigData() {
    if (this.currentService) {
      this.configForm.patchValue(this.currentService)
      const _redirect_uris = this.currentService.redirect_uris

      this.redirectUrisFormArray.clear()
      _redirect_uris.forEach(uri => {
        const group = this.newUriFormGroup(uri, true)
        this.redirectUrisFormArray.push(group)
      })

      if (!_redirect_uris.length) {
        const uriItem = this.newUriFormGroup(null, false)
        this.redirectUrisFormArray.push(uriItem)
      }

      const _js_origin_uris = this.currentService.js_origin_uris

      this.jsOriginUrisFormArray.clear()
      _js_origin_uris.forEach(uri => {
        const group = this.newUriFormGroup(uri, true)
        this.jsOriginUrisFormArray.push(group)
      })
      if (!_js_origin_uris.length) {
        const jsItem = this.newUriFormGroup(null, false)
        this.jsOriginUrisFormArray.push(jsItem)
      }

      this.configForm.markAsPristine()
    }
  }

  get jsOriginUrisFormArray(): FormArray {
    return this.configForm.get('_js_origin_uris') as FormArray
  }
  get redirectUrisFormArray(): FormArray {
    return this.configForm.get('_redirect_uris') as FormArray
  }

  async save() {
    const formValue = this.configForm.value
    const id = this.currentService._id
    const service_code = this.currentService.service_code
    const tos_url = formValue.tos_url.trim()
    const policy_url = formValue.policy_url.trim()
    const redirect_uris = (formValue._redirect_uris as [])
      .filter((x: { uri }) => x.uri !== null)
      .map((x: { uri }) => x.uri.trim())
      .join(',')

    const js_origin_uris = (formValue._js_origin_uris as [])
      .filter((x: { uri }) => x.uri !== null)
      .map((x: { uri }) => x.uri.trim())
      .join(',')

    const request: IServiceUpdateRequest = { id, service_code, tos_url, policy_url, redirect_uris, js_origin_uris }

    const response = await this.appService.putApplicationServiceUpdate(request).toPromise()

    if (response?.success) {
      this.configForm.markAsPristine()
      this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', true)
    } else if (response?.error) {
      const error = response?.error
      this.setMessage(ERROR_MESSAGE, error.message, true)
    }
  }

  setUrlId(type: string, num: number) {
    return `${type}URI${num}`
  }

  setUrlLabel(type: string, num: number) {
    return num >= 1 ? `URL de ${type} - ${num + 1}` : `URL de ${type}`
  }
}

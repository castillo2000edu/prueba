import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { IAppOrganization } from '../../../../_interfaces/application.interface'
import { UsuarioModel } from 'src/app/_models/usuario.model'
import { Subject } from 'rxjs'
import { MessageService } from '../../../../_shared/services/message.service'
import { SecurityService } from 'src/app/_services/security.service'
import { ApplicationService } from '../../../../_services/application.service'
import { BusyService } from 'src/app/_services/busy.service'
import { ActivatedRoute, Router } from '@angular/router'
import { RegexPatterns } from 'src/app/_shared/regexPatterns'
import { takeUntil } from 'rxjs/operators'
import { Base64 } from 'js-base64'
import { IApplicationModel } from '../../../../_models/application.model'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { BaseMessageComponent } from 'src/app/_shared/components/base-message/base-message.component'
@Component({
  selector: 'app-new-application',
  templateUrl: './new-application.component.html',
  styleUrls: ['./new-application.component.scss']
})
export class NewApplicationComponent extends BaseMessageComponent implements OnInit {
  formGroup: FormGroup | null
  isLoading = false
  entities: IAppOrganization[] | null

  file: File | null
  url: string | ArrayBuffer | null

  showPage: boolean

  private user: UsuarioModel | null
  private _unsubscribeAll: Subject<any>

  constructor(
    private _messageService: MessageService,
    private formBuilder: FormBuilder,
    private _securityService: SecurityService,
    private _applicationService: ApplicationService,
    private _busyService: BusyService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    super(activatedRoute)
    this._unsubscribeAll = new Subject()
  }

  ngOnInit(): void {
    this.createForm()
    this.listenLoadingEvent()
    this.getUser()
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      name: [null, [Validators.required, Validators.maxLength(50)]],
      description: [null, [Validators.required, Validators.maxLength(150)]],
      url: [null, [Validators.required, Validators.pattern(RegexPatterns.URL)]],
      organization_code: [null, [Validators.required]],
      logo: [null]
    })

    this.showPage = true
  }

  async save() {
    if (this.formGroup.valid) {
      let { name, description, url, organization_code } = this.formGroup.value
      name = Base64.encode(name)
      description = Base64.encode(description)
      const request: IApplicationModel = { name, description, url, organization_code, logo: this.file }
      const response = await this._applicationService.postApplicationAppInsert(request).toPromise()

      if (response?.success) {
        this.setMessage(SUCCESS_MESSAGE, 'Registro guardado con éxito', false)
        this.cancel()
      } else if (response?.error) {
        const error = response?.error
        this.setMessage(ERROR_MESSAGE, error.message, true)
      }
    }
  }

  cancel() {
    const message = this.textMessage
    const typeMessage = this.typeMessage
    this.router.navigate(['application/list'], { state: { message, typeMessage } })
  }

  isInValidField(strField: string): boolean {
    const field = this.formGroup.get(strField)
    return field.invalid && field.touched
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  private getUser() {
    this._securityService.currentUser$.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.user = x
      const serviceParam = this.user.extra?.developer?.join(',')
      if (serviceParam) {
        this.getOrganizations(serviceParam)
      }
    })
  }

  private async getOrganizations(code: string) {
    const response = await this._applicationService.getApplicationOrganizationCode(code).toPromise()
    if (response?.success) {
      this.entities = response?.Items
      if (this.entities.length === 1) {
        this.formGroup.controls['organization_code'].setValue(this.entities[0].code)
      }
    }
  }

  setTitle() {
    return 'Nueva aplicación'
  }

  setSubtitle() {
    return 'Completa los campos para crear una nueva aplicación.'
  }
}

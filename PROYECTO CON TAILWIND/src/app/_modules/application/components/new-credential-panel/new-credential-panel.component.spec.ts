import { ComponentFixture, TestBed } from '@angular/core/testing'

import { NewCredentialPanelComponent } from './new-credential-panel.component'

describe('NewCredentialPanelComponent', () => {
  let component: NewCredentialPanelComponent
  let fixture: ComponentFixture<NewCredentialPanelComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NewCredentialPanelComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCredentialPanelComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

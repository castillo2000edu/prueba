import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { ApplicationService } from 'src/app/_services/application.service'
import { BusyService } from 'src/app/_services/busy.service'
import { PopupConfirmComponent } from 'src/app/_shared/components/popup-confirm/popup-confirm.component'
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from 'src/app/_shared/constants'
import { MessageService } from 'src/app/_shared/services/message.service'

@Component({
  selector: 'app-new-credential-panel',
  templateUrl: './new-credential-panel.component.html',
  styleUrls: ['./new-credential-panel.component.scss']
})
export class NewCredentialPanelComponent implements OnInit {
  @Input()
  serviceId: string | null

  @Output()
  newCredentialEvent: EventEmitter<any> | null

  isLoading: boolean
  private _unsubscribeAll: Subject<any>

  constructor(
    private _busyService: BusyService,
    private _messageService: MessageService,
    private dialog: MatDialog,
    private _srvService: ApplicationService
  ) {
    this.newCredentialEvent = new EventEmitter<any>()

    this._unsubscribeAll = new Subject()
  }

  ngOnInit(): void {
    this.listenLoadingEvent()
  }

  private listenLoadingEvent() {
    this._busyService.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {
      this.isLoading = x
    })
  }

  async regenerate() {
    if (this.serviceId) {
      const dialogRef = this.dialog.open(PopupConfirmComponent, {
        data: {
          title: `Generar credenciales`,
          question: `¿Estás seguro de generar las credenciales?`
        },
        width: '98%',
        maxWidth: '450px'
      })

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.generateCredential()
        }
      })
    }
  }

  private async generateCredential() {
    const response = await this._srvService.postApplicationServiceRegenerate(this.serviceId).toPromise()
    if (response.success) {
      this.newCredentialEvent.emit({ success: true, messageText: 'Se generó credenciales correctamente.' })
    } else if (response?.error) {
      const error = response?.error
      this.newCredentialEvent.emit({ success: false, messageText: error.message })
    }
  }
}

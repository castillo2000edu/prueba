const { boxShadow, colors, minWidth, maxWidth, minHeight, scale, padding, width } = require("tailwindcss/defaultTheme");

module.exports = {
  important: true,
  purge: {
    enabled: true,
    content: ["./src/**/*.{html,scss,ts}"],
  },
  theme: {
    extend: {
      colors: {
        red: {
          ...colors.red,
          100: '#ffeff0',
          700: "#CF000B"
          
        },
        blue: {
          ...colors.blue,
          100: '#E6F2F8',
          700: '#0056ac',
          900: '#024A93',
        },
        gray: {
          ...colors.gray,
          50: '#4D4D4D',
          100: '#F6F7F9',
          200: '#DEE3EA',
          400: '#F2F5F8',
          450: '#CACFD3',
          460: '#CBDAE8',
          470: '#9CA3AF',
          500: '#6F777B',
          600: '#919191',
          900: '#26292E'
        },
        green: {
          ...colors.green,
          100: '#c9f0d8',
          500: '#076725',
          600: "#1b9d6b"
        },
      },
      boxShadow: {
        ...boxShadow,
        'cont-btn-login': '2px 3px 23px 0px rgba(203,218,232,1)',
        'cont-btn-login-hover': '3px 3px 32px -4px rgba(137, 160, 191, 0.6)',
        "active-menu": "0 4px 11px rgba(138, 142, 146, 0.15)",
      },
      spacing: {
        '1px': '1px',
        '2px': '2px',
        '6px': '6px',
        '5/4': '5px',
        '138px': '138px',
        '49px': '49px',
        18: '4.5rem',
        '7.5': '1.875rem'
      },
      minWidth: {
        ...minWidth,
        4: '4rem',
        '100px': '100px',
        '733px': '733px'
      },
      maxWidth: {
        ...maxWidth,
        12: '12rem',
      },
      minHeight: {
        ...minHeight,
        3: '3rem',
      },
      scale: {
        ...scale,
        '2/3': '.666',
        '4/5': '.8',
        '1/5': '.2'
      },
      width: {
        ...width,
        '440px': '440px'
      },
    },
  },
  variants: {
    extend: {}
  },
  plugins: [],
  corePlugins: {
    preflight: false,
  },
};
